<?php
require('Controller.php');
require (__ROOT__.'/model/Utilisateur.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class AddAdministrateur extends Controller{

    
    public function get($request){
        $this->render('add_admin',[]);
    }

    
    public function post($request){
        try{
            $user = UtilisateurDAO::getInstance();
            $st = new Utilisateur();

            if ($user->find($request['mail']) == null){
                $mail = $request['mail'];
                $nom = $request['nom'];
                $prenom = $request['prenom'];
                $nat = $request['nationalite'];
                $codePostal = floatval($request['codePostal']);
                $adresse = $request['adresse'];
                $mdp = $request['mdp'];
                $dateNaissance= $request['date'];
                $complAdr= $request['complAdr'];
                $ville= $request['ville'];
                $pays= $request['pays'];
                $action= "Renouvellement";
                $sexe= $request['sexe'];
                $tel= $request['tel'];
                $mobile= $request['mobile'];
                $prevNom= $request['prevNom'];
                $prevPrenom = $request['prevPrenom'];
                $prevTel = $request['prevTel'];
                $prevMail= $request['prevMail'];
                $numLicence= '123456';
                $typeLicence= $request['typeLicence'];
                $assurance= $request['typeAssurance'];
                $opAssur= $request['optAssur'];
                $ski = $request['ski'];
                $slackline = $request['slackline'];
                $trail = $request['trail'];
                $vtt = $request['vtt'];
                $statut = "inscrit";
                $id = 0;


                
                $st->init($id,$action,$nom, $prenom,$dateNaissance,$sexe,$nat,$adresse,$complAdr,$codePostal,$ville,$pays,$tel,$mobile,$mail,$mdp,1,$prevNom,$prevPrenom,$prevTel,$prevMail,$numLicence,$typeLicence,$assurance,$ski,$slackline,$trail,$vtt,$opAssur,$statut);

                $user->insert($st);


                $user = UtilisateurDAO::getInstance();
                $st = $user->findAdmin();

                $this->render('manage_admin',['message'=>$st]);

            }else{
                $this->render('error_admin',['message'=>"mail déjà utilisé"]);
            }
          

        }catch(Exception $e){
            $this->render('error_admin',['message'=>$e->getMessage()]);

        }
        

    }

}

?>
