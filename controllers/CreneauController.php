<?php
session_start();
require('Controller.php');
require (__ROOT__.'/model/Creneaux.php');
require (__ROOT__.'/model/CreneauxDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');
require (__ROOT__.'/model/AppartenanceDAO.php');
require (__ROOT__.'/model/Appartenance.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/Utilisateur.php');

class CreneauController extends Controller{

    
    public function get($request){
        $creneaux = CreneauxDAO::getInstance();
        $appartenance = AppartenanceDAO::getInstance();
        $user = UtilisateurDAO::getInstance();
        $id = $user->find($_SESSION['mail'])[0]->getId();
        $nums = $appartenance->getCreneauxUser($id);
        $st;
        foreach($nums as $num){
        	if (!isset($st)){
        		$st = $creneaux->find($num->getLeCreneau());
        	}else{
        	    $st = array_merge($st,$creneaux->find($num->getLeCreneau()));
        	}
        }
        if (!isset($st)){
        	$st = null;
        }
        $this->render('tab_creneau',['st'=>$st]);
    }

    
    public function post($request){
        try{
            $connect = SQLiteConnection::getInstance()->getConnection(); 
            $creneau = CreneauxDAO::getInstance();
            $st = new Creneaux();

            $num = $request['id'];
            $jour = $request['jour'];
            $horaires = $request['horaires'];
            $division = $request['division'];

            $st->init($num,$jour,$horaires,$division);

            $creneau->insert($st);

            $this->render('accueil_connect_admin',[]);


        }catch(Exception $e){
            $this->render('error',['message'=>$e->getMessage()]);

        }
        

    }

}

?>
