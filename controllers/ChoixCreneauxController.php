<?php
require('Controller.php');
require (__ROOT__.'/model/Utilisateur.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/Creneaux.php');
require (__ROOT__.'/model/CreneauxDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');
require (__ROOT__.'/model/AppartenanceDAO.php');
require (__ROOT__.'/model/Appartenance.php');

class ChoixCreneauxController extends Controller{

    
    public function get($request){
       
        $dateAct = $request['date'];
        $dateTime = new DateTime($dateAct);
        $dates = $dateTime->format('d/m/Y');
        $year = $dateTime->format('Y');
        $date = date("Y",time());
        $age = $date - $year;
        $cren = CreneauxDAO::getInstance();
        $user = $request;

        if($age < 4){
            $this->render('error',['message'=> 'Grimpeur trop jeune']);
        }else if($age < 8){
            $div = 'U8';
            $st = $cren->findByDiv($div);
            $this->render('choix_creneau',['liste' =>$st,'user'=>$user]);
        }else if ($age < 10 && $age >= 8){
            $div = 'U10';
            $st = $cren->findByDiv($div);
            $this->render('choix_creneau',['liste' =>$st,'user'=>$user]);
        }else if ($age < 12 && $age >= 10){
            $div = 'U12';
            $st = $cren->findByDiv($div);
            $this->render('choix_creneau',['liste' =>$st,'user'=>$user]);
        }else if ($age < 14 && $age >= 12){
            $div = 'U14';
            $st = $cren->findByDiv($div);
            $this->render('choix_creneau',['liste' =>$st,'user'=>$user]);
        }else if ($age < 16 && $age >= 14){
            $div = 'U16';
            $st = $cren->findByDiv($div);
            $this->render('choix_creneau',['liste' =>$st,'user'=>$user]);
        }else if ($age < 18 && $age >=16){
            $div = 'U18';
            $st = $cren->findByDiv($div);
            $this->render('choix_creneau',['liste' =>$st,'user'=>$user]);
        }else if ($age < 20 && $age >= 18){
            $div = 'U20';
            $st = $cren->findByDiv($div);
            $this->render('choix_creneau',['liste' =>$st,'user'=>$user]);
        }else{
            $div = '+20';
            $st = $cren->findByDiv($div);
            $this->render('choix_creneau',['liste' =>$st,'user'=>$user]);
        }
    }

}

?>
