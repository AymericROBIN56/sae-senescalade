<?php
require('Controller.php');
require (__ROOT__.'/model/Creneaux.php');
require (__ROOT__.'/model/CreneauxDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class DeleteCreneaux extends Controller{
    
    public function post($request){
        try{
            $creneau = CreneauxDAO::getInstance();
            $st = $creneau->find($request['numero']);

            if($st != null){
                $creneau->delete($request['numero']);


                $st = $creneau->findAll();

                $this->render('manage_creneaux',['message'=>$st]);
            }else{
                $this->render('error_admin',['message'=>"numéro introuvable"]);
            }

            
        }catch(Exception $e){
            $this->render('error_admin',['message'=>$e->getMessage()]);

        }
        

    }

}

?>
