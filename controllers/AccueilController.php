<?php
session_start();
require('Controller.php');
require (__ROOT__.'/model/Utilisateur.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class AccueilController extends Controller{

    
    public function get($request){
        $dao = UtilisateurDAO :: getInstance();
        $user = $dao->find($_SESSION['mail']);
        
        if ($user[0]->getAdmin() == 0){
            $this->render('accueil_connect',[]);
        }else if ($user[0]->getAdmin() == 1){
            $this->render('accueil_connect_admin',[]);
        }
    }

}

?>
