<?php
session_start();
require('Controller.php');
require (__ROOT__.'/model/Creneaux.php');
require (__ROOT__.'/model/CreneauxDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class ManageCreneaux extends Controller{

    
    public function get($request){
        $creneaux = CreneauxDAO::getInstance();
        $st = $creneaux->findAll();
        $this->render('manage_creneaux',['message' => $st]);
        
    }

}

?>
