<?php
session_start();
require('Controller.php');
require (__ROOT__.'/model/Creneaux.php');
require (__ROOT__.'/model/CreneauxDAO.php');
require (__ROOT__.'/model/Appartenance.php');
require (__ROOT__.'/model/AppartenanceDAO.php');
require (__ROOT__.'/model/Utilisateur.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class ConfirmDesinscr extends Controller{

    
    public function get($request){
        $creneau = CreneauxDAO::getInstance();
        $st = $creneau->find($request['desinscr']);
        $this->render('confirm_descr',['st'=>$st]);
    }

    public function post($request){
        $appartenance = AppartenanceDAO::getInstance();
        $user = UtilisateurDAO::getInstance();

        $u = $user->find($_SESSION['mail']);
        $appartenance->delete($u[0]->getId(),$request['numero']);
        $this->render('accueil_connect',[]);
    }

}

?>
