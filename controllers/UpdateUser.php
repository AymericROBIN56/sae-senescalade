<?php
session_start();
require('Controller.php');
require (__ROOT__.'/model/Utilisateur.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class UpdateUser extends Controller{

    
    public function get($request){
        $dao = UtilisateurDAO :: getInstance();
        $user = $dao->find($_SESSION['mail']);  
        $this->render('update_form',['nat' =>$user[0]->getNationalite(),'adresse' =>$user[0]->getAdresse(),
         'complAdr' =>$user[0]->getComplAdr(),'codePostal' =>$user[0]->getCodePostal(),'ville' =>$user[0]->getVille(),
        'pays' =>$user[0]->getPays(),'mail' =>$user[0]->getMail(),'mdp' =>$user[0]->getMdp(),
        'typeLicence' =>$user[0]->getTypeLicence(),'typeAssurance' =>$user[0]->getAssurance(),
        'opAssur' =>$user[0]->getOpAssurance()]);
    }

    
    public function post($request){
        try{
            $connect = SQLiteConnection::getInstance()->getConnection(); 
            $dao = UtilisateurDAO::getInstance();
            $user = $dao->find($_SESSION['mail']);
            $st = new Utilisateur();

            $dateNaissance = $user[0]->getDate();
            $prenom = $user[0]->getPrenom();
            $nom = $user[0]->getNom();
            $sexe = $user[0]->getSexe();            
            $mail = $request['mail'];
            $_SESSION['mail'] = $mail;
            $nat = $request['nationalite'];
            $codePostal = floatval($request['codePostal']);
            $adresse = $request['adresse'];
            $mdp = $request['mdp'];
            $creneau = $request['creneau'];
            $complAdr= $request['complAdr'];
            $ville= $request['ville'];
            $admin= $user[0]->getAdmin();;
            $pays= $request['pays'];
            $action= $user[0]->getAction();
            $tel= $request['tel'];
            $numLicence = $user[0]->getNumeroLicence();
            $mobile= $request['mobile'];
            $prevNom= $request['prevNom'];
            $prevPrenom= $request['prevPrenom'];
            $prevTel= $request['prevTel'];
            $prevMail= $request['prevMail'];
            $typeLicence= $user[0]->getTypeLicence();
            $assurance= $request['assur'];
            $opAssur= $request['opAssur'];
            $ski = $request['ski'];
            $slackline = $request['slackline'];
            $trail = $request['trail'];
            $vtt = $request['vtt'];


            $st->init($action,$nom, $prenom,$dateNaissance,$sexe,$nat,$adresse,$complAdr,$codePostal,$ville,$pays,$tel,$mobile,$mail,$mdp,$admin,$prevNom,$prevPrenom,$prevTel,$prevMail,$numLicence,$typeLicence,$assurance,$ski,$slackline,$trail,$vtt,$opAssur,$creneau);

            $dao->update($st);

            $this->render('update_form',[]);

        }catch(Exception $e){
            $this->render('error',['message' =>$e->getMessage()]);

        }
        

    }

}

?>
