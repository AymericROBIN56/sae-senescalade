<?php
require('Controller.php');
require (__ROOT__.'/model/Utilisateur.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class DeleteUser extends Controller{
    
    public function post($request){
        try{
            $user = UtilisateurDAO::getInstance();
        $id = $request['id'];
            $user->delete($id);
            $st = $user->findNoAdmin();
            $this->render('manage_user',['message'=>$st]);

            
        }catch(Exception $e){
            $this->render('error_admin',['message'=>$e->getMessage()]);

        }
        

    }

}

?>
