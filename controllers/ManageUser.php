<?php
session_start();
require('Controller.php');
require (__ROOT__.'/model/Utilisateur.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class ManageUser extends Controller{

    
    public function get($request){
        $user = UtilisateurDAO::getInstance();
        $st = $user->findNoAdmin();
        $this->render('manage_user',['message' => $st]);
        
    }

}

?>
