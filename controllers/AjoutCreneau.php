<?php
require('Controller.php');
require (__ROOT__.'/model/Creneaux.php');
require (__ROOT__.'/model/CreneauxDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class AjoutCreneau extends Controller{

    
    public function get($request){
        $this->render('form_creneaux',[]);
    }

    
    public function post($request){
        try{
            $connect = SQLiteConnection::getInstance()->getConnection(); 
            $creneau = CreneauxDAO::getInstance();
            $st = new Creneaux();

            $num = 0;
            $jour = $request['jour'];
            $horaireDeb = $request['horaireDeb'];
            $horaireFin = $request['horaireFin'];
            $division = $request['division'];
            $capacite = $request['capacite'];
            $cat = $request['cat'];
            $prix = intval($request['prix']);
            $descr = $request['descr'];

            $st->init($num,$jour,$horaireDeb,$horaireFin,$division,$cat,$prix,$descr,$capacite,0);

            $creneau->insert($st);


            $creneaux = CreneauxDAO::getInstance();
            $st = $creneaux->findAll();

            $this->render('manage_creneaux',['message'=>$st]);


        }catch(Exception $e){
            $this->render('error',['message'=>$e->getMessage()]);

        }
        

    }

}

?>
