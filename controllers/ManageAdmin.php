<?php
session_start();
require('Controller.php');
require (__ROOT__.'/model/Utilisateur.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class ManageAdmin extends Controller{

    
    public function get($request){
        $user = UtilisateurDAO::getInstance();
        $st = $user->findAdmin();

        $this->render('manage_admin',['message' => $st]);
        
    }

}

?>
