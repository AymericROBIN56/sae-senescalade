<?php
session_start();
require('Controller.php');
require (__ROOT__.'/model/SQLiteConnection.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/CreneauxDAO.php');
require (__ROOT__.'/model/Creneaux.php');
require (__ROOT__.'/model/Utilisateur.php');
require (__ROOT__.'/model/Appartenance.php');
require (__ROOT__.'/model/AppartenanceDAO.php');

class UserAddController extends Controller{

    public function get($user){
    $mail = $user['mail'];
    $user = UtilisateurDAO::getInstance();
    if($user->find($mail) == null){
        $this->render('form_inscr',['action' =>"Inscription",'mail' => $mail]);
    }else{
        $this->render('form_inscr',['action' =>"Renouvellement",'mail' => $mail]);
    }
    }

    
    public function post($request){
        try{
            $users = UtilisateurDAO::getInstance();
            $creneaux= CreneauxDAO::getInstance();
            $st = new Utilisateur();
            $stApp= new Appartenance();
            $appartenance = AppartenanceDAO::getInstance();

                $user = $_SESSION['user'];
                
            
                $mail = $user['mail'];
                $nom = $user['nom'];
                $prenom = $user['prenom'];
                $nat = $user['nationalite'];
                $codePostal = floatval($user['codePostal']);
                $adresse = $user['adresse'];
                $mdp = $user['mdp'];
                $dateNaissance= $user['date'];
                $complAdr= $user['complAdr'];
                $ville= $user['ville'];
                $pays= $user['pays'];
                $action= $user['action'];
                $sexe= $user['sexe'];

                $tel= $user['tel'];
                $mobile= $user['mobile'];
                $prevNom= $user['prevNom'];
                $prevPrenom = $user['prevPrenom'];
                $prevTel = $user['prevTel'];
                $prevMail= $user['prevMail'];
                $typeLicence= $user['typeLicence'];
                $assurance= $user['typeAssurance'];
                $opAssur= $user['optAssur'];
                $ski = $user['ski'];
                $slackline = $user['slackline'];
                $trail = $user['trail'];
                $vtt = $user['vtt'];
                $creneau = $request['creneau'];
                $statut = 'placé';
                $id = 0;


                unset($_SESSION['user']);
                session_destroy();

                if($action == "Renouvellement"){
                    $numLicence= $user['numLicence'];
                }else{
                    $numLicence = null;
                }



                $st->init($id,$action,$nom, $prenom,$dateNaissance,$sexe,$nat,$adresse,$complAdr,$codePostal,$ville,$pays,$tel,$mobile,$mail,$mdp,0,$prevNom,$prevPrenom,$prevTel,$prevMail,$numLicence,$typeLicence,$assurance,$ski,$slackline,$trail,$vtt,$opAssur,$statut);
                $users->insert($st);

                $id = $users->findByIdentite($mail,$nom,$prenom);

                if ($creneaux->capacite($creneau) > 0){
                    $attente = 0;
                    $stApp->init($id,$creneau,$attente);
                    $appartenance->insert($stApp);
                }else{
                    $users->changeStatut("attente",$id);
                    $attente = $creneaux->attente($creneau) + 1;
                    $stApp->init($id,$creneau,$attente);
                    $appartenance->insert($stApp);
                }
                if ($appartenance->getAttente($creneau,$id) == 0){
                    $this->render('user_connect_form',[]);
                }else{
                    $this->render('file_attente',['message'=>$attente,'creneau'=>$creneau]);
                }

        }catch(Exception $e){
            $this->render('error',['message'=> $e->getMessage()]);
        }

    }
}

?>
