<?php
require('Controller.php');
require (__ROOT__.'/model/Utilisateur.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class DeleteAdmin extends Controller{

    
    public function post($request){
        try{
            $user = UtilisateurDAO::getInstance();
            $st = $user->findPren($request['nom'],$request['prenom']);
            $user->delete($st);
            
            
            $st = $user->findAdmin();
            $this->render('manage_admin',['message'=>$st]);

            
        }catch(Exception $e){
            $this->render('error_admin',['message'=>$e->getMessage()]);

        }
        

    }

}

?>
