<?php
session_start();
require('Controller.php');
require (__ROOT__.'/model/Utilisateur.php');
require (__ROOT__.'/model/UtilisateurDAO.php');
require (__ROOT__.'/model/SQLiteConnection.php');

class ConnectUserController extends Controller{
    private $user;
    private UtilisateurDAO $dao;

    
    public function get($request){
        unset($_SESSION["mail"]);
        unset($_SESSION['user']);
        $this->render('user_connect_form',[]);
    }

    
    public function post($request){
        try{
            $dao = UtilisateurDAO :: getInstance();
            $user = $dao->find($request['email']);
            if ($user != null){
                $_SESSION['mail'] = $request["email"];
                if($user[0]->getMdp() == $request['mdp'] && $user[0]->getAdmin() == 1){
                    $this->render('accueil_connect_admin',[]);
                }else if ($user[0]->getMdp()== $request['mdp']){
                    $this->render('accueil_connect',[]);
                }else{
                    $this->render('error',['message' =>"Mot de passe invalide"]);
                }
            }else{
                $this->render('error',['message' =>"Compte introuvable"]);
            }
        }catch(Exception $e){
            $this->render('error',['message' =>$e->getMessage()]);

        }
        

    }

}

?>
