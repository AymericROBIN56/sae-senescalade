Create Table Creneaux(
	numero integer primary key autoincrement,
	jour text CHECK (jour IN ('Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche')),
	horaireDeb text CHECK (horaireDeb LIKE '_%H' OR horaireDeb LIKE '_%H_%' AND horaireDeb > '00H00' AND horaireDeb < '23H59'),
	horaireFin text CHECK (horaireFin LIKE '_%H' OR horaireFin LIKE '_%H_%' AND horaireFin > '00H00' AND horaireFin < '23H59'),
	division text  CHECK (division IN ('U8','U10','U12','U14','U16','U18','U20','+20')),
	categorie text CHECK (categorie IN ('Babygrimpe','Compétition','Jeunes','Adultes')),
	capacite integer CHECK (capacite >= 0),
	description text,
	prix integer CHECK (prix > 0),
	fileAttente integer CHECK (fileAttente >=0) 
);
	
	

courriel,action,nom,prenom,dateNaissance,sexe,nationalite,adresse,complAdr,codePostal,ville,pays,telephone,mobile,mdp,admin,personnePrevenirNom,personnePrevenirPrenom,personnePrevenirTel,personnePrevenirCourriel,
numeroLicence,typeLicence,assurance,ski,slackline,trail,vtt,opAssurance,statut

	
Create Table Utilisateur (
	id integer PRIMARY KEY autoincrement,
	courriel text CHECK (courriel LIKE '_%._%@_%._%' OR courriel LIKE '_%@_%._%') UNIQUE,
	action text CHECK (action IN ('Inscription','Renouvellement')),
	nom text,
	prenom text,
	dateNaissance text CHECK (dateNaissance < strftime(CURRENT_DATE,'%d/%m/%Y')),
	sexe text CHECK (sexe IN ('H','F')),
	nationalite text NOT NULL,
	adresse text,
	complAdr text,
	codePostal integer,
	ville text,
	pays text NOT NULL, 
	telephone text,
	mobile text,
	mdp text CHECK (LENGTH(mdp) >= 7),
	admin integer CHECK (admin == 0 OR admin == 1),
	personnePrevenirNom text,
	personnePrevenirPrenom text,
	personnePrevenirTel text,	
	personnePrevenirCourriel text CHECK (personnePrevenirCourriel LIKE '_%._%@_%._%' OR personnePrevenirCourriel LIKE '_%@_%._%' OR personnePrevenirCourriel = null),
	numeroLicence integer CHECK (LENGTH(numeroLicence) = 6 OR LENGTH(numeroLicence) = 0),
	typeLicence text CHECK (typeLicence IN ('J','F','A')),
	assurance text CHECK (assurance IN ('RC','B','B+','B++')),
	ski text CHECK (ski IN ('OUI','NON')),
	slackline text CHECK (slackline IN ('OUI','NON')),
	trail text CHECK (trail IN ('OUI','NON')),
	vtt text CHECK (vtt IN ('OUI','NON')),
	opAssurance text CHECK (opAssurance IN ('IJ1','IJ2','IJ3','NON')),
	statut text CHECK (statut IN ('attente ','placé','inscrit'))
);


CREATE TABLE Appartenance(
	userID integer,
	leCreneau integer,
	numeroAttente integer,
	FOREIGN KEY (userID) REFERENCES Utilisateur(id),
	FOREIGN KEY (leCreneau) REFERENCES Creneaux(numero),
	PRIMARY KEY(userId,leCreneau)
);		



CREATE TRIGGER ajout1
BEFORE INSERT ON Appartenance
FOR EACH ROW
BEGIN
	UPDATE Creneaux
	SET fileAttente = fileAttente + 1
	WHERE numero = NEW.leCreneau AND capacite = 0;
END;	

CREATE TRIGGER ajout2
BEFORE INSERT ON Appartenance
FOR EACH ROW
BEGIN
	UPDATE Creneaux
	SET capacite = capacite - 1
	WHERE numero = NEW.leCreneau AND capacite > 0;
END;


CREATE TRIGGER del1
AFTER DELETE ON Appartenance
FOR EACH ROW
BEGIN	
	UPDATE Creneaux
	SET fileAttente = fileAttente - 1
	WHERE numero = OLD.leCreneau AND capacite = 0;
END;


CREATE TRIGGER del2
AFTER DELETE ON Appartenance
FOR EACH ROW
BEGIN
	UPDATE Creneaux
	SET capacite = capacite + 1
	WHERE numero = OLD.leCreneau AND capacite > 0;
END;


CREATE TRIGGER del3
AFTER DELETE ON Appartenance
FOR EACH ROW
BEGIN	
	UPDATE Appartenance
	SET numeroAttente = numeroAttente - 1
	WHERE leCreneau = OLD.leCreneau AND numeroAttente > OLD.numeroAttente AND numeroAttente > 0;
END;



Admin par défaut : 

8|admin@gmail.com|Renouvellement|admin|admin|null|H|FR|null||0|null|FR|||admin00|1|||||123456|A|B|NON|NON|NON|NON|NON|inscrit



	
	
	 
