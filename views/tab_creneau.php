<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
    <title>Emploi du temps</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid black;
        }

        th, td {
            padding: 10px;
            text-align: center;
        }

        th {
            background-color: #f2f2f2;
        }
    </style>
</head>

<body>
    <header>
        <?php
            include ("headerClient.php");
        ?>
    </header>
<form class="mb-3 mt-md-4" action='/confirm_desinscr' method = "GET">
    <div class="mb-3">
            <label for="jour" class="form-label">Se désinscrire d'un créneau : </label>
                <select name="desinscr" class="form-select" id="désinscr">
                    <?php 
                    foreach($data['st'] as $crenau){
                        $creneau = $crenau->getNumero();
                        echo "<option value=$creneau>$creneau</option>";
                    }
                    ?>
                    
                </select>
    </div>
    <div class="d-grid">
        <button type="submit" class="btn btn-outline-dark">Confirmer</button>
    </div>
</form>


    <h2>Emploi du temps</h2>


    <div class="button">
    </div>
    <table align = "center" border = "1" cellpadding = "3" cellspacing = "2">  
                <tr>  
                <td> Numéro </td>  
                <td> Jour </td>  
                <td> Horaire de début </td> 
                <td> Horaire de fin </td>  
                <td>  Division </td>
                <td> Catégorie </td> 
                <td>  Capacite </td>
                <td>  File attente </td> 
                </tr>  
                <?php 
                $x = 10;  
                $price = 35;  
                $quatity = 1;
		
		if($data['st'] != null){
		
                foreach($data['st'] as $array){
                  echo "<tr>";
                  $numero = $array->getNumero();
                  $jour = $array->getJour();
                  $horaireDeb = $array->getHoraireDeb();
                  $horaireFin = $array->getHoraireFin();
                  $divison = $array->getDiv();
                  $cat = $array->getCat();
                  $capacite = $array->getCapacite();
                  $file = $array->getFileAttente();
                  echo "<td> $numero </td>";  
                  echo "<td> $jour </td>";  
                  echo "<td> $horaireDeb </td>";
                  echo "<td> $horaireFin </td>";  
                  echo "<td> $divison </td>";  
                  echo "<td> $cat </td>";
                  echo "<td> $capacite </td>";
                  echo "<td> $file </td>";    
                    echo "</tr>";  
                  $x += 10;  
                  $quatity++;  
                }
                }
                ?>    
                </table>

</body>

</html>
