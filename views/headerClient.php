<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }
        .headerd {
            padding: 10px 0;
            text-align: center;
            color:black;
        }
        headerd h1 {
            margin: 0;
            padding: 0;
        }
        nav {
            float: right;
            margin-right: 20px;
        }
        nav a {
            color: black;
            text-decoration: none;
            padding: 0 15px;
            background-color:whitesmoke
        }
        nav a:hover {
            text-decoration: none;
            color:whitesmoke
        }
        .clearfix {
            clear: both;
        }


        a{  padding: 20px 40px;
            font-size: 24px;
            background-color: whitesmoke;
            border-radius: 5px;
            transition: background-color 1s ease;
            outline: none;
        }

        a:hover{
            background-color: #670b22;
            color: whitesmoke;
        }
        #profil{
            position:absolute;
            left : 10px;
        }
        .center{
            position:absolute;
            left: 45%;
        }

        
    </style>
</head>
<body>
    <header tabindex="0" class="headerd">
        <img src="https://senescalade.bzh/wp-content/uploads/2022/07/logo.png">
        <h1>Espace Client</h1>
        <nav>
            <div class="center">
                <a tabindex="0" href="/accueil">Accueil client</a>
            </div>
            <a class="act" id="profil" tabindex="0" href="/update">Modifier mon profil</a>
            <a tabindex="0" href="/creneau">Voir mon créneau</a>
            <a tabindex="0" href="/disconnect">Se déconnecter</a>
        </nav>
        <div class="clearfix"></div>
    </header>
</body>
</html>
﻿
