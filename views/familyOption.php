<!DOCTYPE html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
        <TITLE class="Option famille"></TITLE>
    </head>
    <body>
      <header>
        <?php
        include("header.php")
        ?>
      </header>
    <div>
      <h2 style="text-align : center;">Ajout utilisateurs supplémentaires</h2>
  </div>

  <div style="text-align: center;" id="app">
        <div class="vh-100 d-flex justify-content-center align-items-center">
          <div class="container">
            <div class="row d-flex justify-content-center" >
              <div class="col-12 col-md-8 col-lg-6" >
                <div class="border border-3 border-danger"></div>
                <div class="card bg-white">
                  <div class="card-body p-5">
                      <form @submit.prevent="add">
                        <div v-for="(person, index) in users" :key="index">
                          <input
                            type="text"
                            v-model="person.nom"
                            placeholder="Nom"
                            required
                          />
                          <input
                            type="text"
                            v-model="person.prenom"
                            placeholder="Prénom"
                            required
                          />
                          <div>
                          <input
                            type="date"
                            v-model="person.date"
                            placeholder="Date de naissance"
                            required
                          />
                          </div>
                          <button type="button" class="btn btn-danger" style="margin-top:20px;margin-bottom:20px;" @click="remove(index)">Remove</button>
                        </div>
                        <button type="submit" class="btn btn-success">Ajout d'un utilisateur</button>
                      </form>
                  </div>
                </div>
              </div>
            </div>
            <form @submit.prevent="sendData()" action="/family" method="GET">
        <button type="submit" class="btn btn-success" style="margin-top:20px;">Valider</button>
        </form>
          </div>
        </div>
    </div>


        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script defer src="../js/choix.js"></script>
    </body>
</html>
