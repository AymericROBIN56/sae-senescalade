<!DOCTYPE html>
    <meta charset=”UTF-8”>
    <HEAD>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
      <TITLE class="list">Login</TITLE>
    </HEAD>
    <BODY class="essai">
      <header>
        <?php
          include("header.php");
        ?>
      <div class="vh-100 d-flex justify-content-center align-items-center">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
              <div class="border border-3 border-danger"></div>
              <div class="card bg-white">
                <div class="card-body p-5">
                  <form class="mb-3 mt-md-4" action='/connect' method = "POST">
                    <h2 class="fw-bold mb-2">Connexion</h2>
                    <div class="mb-3">
                      <label for="email" class="form-label ">Adresse mail : </label>
                      <input type="email" name="email" class="form-control" id="email" placeholder="nom@exemple.com" required>
                    </div>
                    <div class="mb-3">
                      <label for="password" class="form-label ">Mot de passe</label>
                      <input type="password" name="mdp" class="form-control" id="password" placeholder="*******" required>
                    </div>
                    <div class="d-grid">
                    <button type="submit" class="btn btn-outline-dark">Connexion</button>
                    </div>
                  </form>
                  <div>
                    <p class="mb-0  text-center">Pas de compte? <a class="btn btn-danger" href="/user_add" role="button">Creer son compte</a></p>
                  </div>
      
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </BODY>
    </HTML>