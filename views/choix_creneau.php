<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
    <title>Choix inscription </title>
    
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid black;
        }

        th, td {
            padding: 10px;
            text-align: center;
        }

        th {
            background-color: #f2f2f2;
        }
        #select {
            text-decoration: none;
            padding: 10px;
            font-family: arial;
            font-size: 1em;
            color: #FFFFFF;
            background-color: #bfbfbf;
            border-radius: 24px;
            -webkit-border-radius: 24px;
            -moz-border-radius: 24px;
            border: 4px solid #bfbfbf;
            box-shadow: 3px 3px 12px #444444;
            -webkit-box-shadow: 3px 3px 12px #444444;
            -moz-box-shadow: 3px 3px 12px #444444;
        }


        #select:hover {
            padding: 10px;
            background-color: #953734;
            border: 4px solid #FFFFFF;
            box-shadow: 1px 1px 4px #777777;
            -webkit-box-shadow: 1px 1px 4px #777777;
            -moz-box-shadow: 1px 1px 4px #777777;
        }
    </style>
</head>

<?php
        if ($data['liste'] == null){ ?>
            <p style="text-align:center;">Creneau vide</p>
            <?php
        }else{
             session_start();
             $_SESSION['user'] = $data['user'];
        }
    
    

?>
<body id="app">
    <header>
        <?php
            include('header.php');
        ?>
    <form class="mb-3 mt-md-4" action="/user_add" method="POST">

        <div class="mb-3">
            <label for="creneau" class="form-label ">Creneau : </label>
            <select id="creneau" name="creneau" placeholder="Choisissez un créneau" class="form-select">
                <?php
                foreach ($data['liste'] as $creneau){
                    $cren = $creneau->getNumero();
                    echo "<option value=$cren>$cren</option>";
                }
                ?>
            </select>
        </div>

        <div class="d-grid">
            <button type="submit" class="btn btn-outline-dark">Valider</button>
        </div>

    </form>


    <h2>Emploi du temps</h2>


    <div class="button">
    </div>
    <table align = "center" border = "1" cellpadding = "3" cellspacing = "2">  
                <tr>  
                <td> Numéro </td>  
                <td> Jour </td>  
                <td> Horaire de début </td>  
                <td> Horaire de fin </td>  
                <td>  Division </td>
                <td> Catégorie </td>  
                <td>  Capacite </td>
                <td>  File attente </td> 
                </tr>  
                <?php 
                $x = 10;  
                $price = 35;  
                $quatity = 1;

                foreach($data['liste'] as $array){
                  echo "<tr>";
                  $numero = $array->getNumero();
                  $jour = $array->getJour();
                  $horaireDeb = $array->getHoraireDeb();
                  $horaireFin = $array->getHoraireFin();
                  $divison = $array->getDiv();
                  $categorie = $array->getCat();
                  $capacite = $array->getCapacite();
                  $file = $array->getFileAttente();
                  echo "<td> $numero</td>";  
                  echo "<td> $jour </td>";  
                  echo "<td> $horaireDeb </td>";  
                  echo "<td> $horaireFin </td>";  
                  echo "<td> $divison </td>";  
                  echo "<td> $categorie </td>";  
                  echo "<td> $capacite </td>";
                  echo "<td> $file </td>";    
                    echo "</tr>";  
                  $x += 10;  
                  $quatity++;  
                }
                ?>    
                </table>
</body>

</html>
