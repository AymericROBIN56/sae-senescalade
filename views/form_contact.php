<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Contact</title>

    <style type="text/css">

        .fa-facebook{
            color: #3549fa;
            font-size: 70px;
        }

        .fa-instagram{
            font-size : 70px;
            color : #c6435e;
        }

        .fa-instagram:hover{
            font-size : 70px;
            color : #c6435e;
        }

        .fa-envelope{
            font-size: 70px;
        }

        .contact{
            display:flex;
            gap : 100px;
            align-items:center;
            text-align:center;
            justify-content:center;
            margin-bottom:50px;     
            margin-top: 10vh;
        }

        .mail{
            display:flex;
            flex-direction:column;
            align-items:center;
        }

        .header{
            text-align:center;
            background-color: #000000;
            color : #ffffff;
            width: 100%;
        }

        .a{
            background-color:white;
        }

        .a:hover{
            background-color:white;
        }



    </style>
</head>
<body class="essai">
        <div class="header">
            <h2>Nos Coordonnées</h2>
        </div>

        <div class = "contact">

            <div class="icone">
                <a class="a" href = "https://www.facebook.com/senescalade/" id="facebook">
                    <i class="fa-brands fa-facebook"></i>
                </a>
            </div>

            <div class="icone">
                <a class="a" href = "https://www.instagram.com/senescalade/">
                    <i class="fa-brands fa-instagram"></i>
                </a>
            </div>


            <div class="mail">
                <i class="fa-solid fa-envelope"></i>
                <p>senescalade@live.fr</p>
            </div>
    </div>
</body>
</html>
