<!DOCTYPE HTML>
<HTML>
    <meta charset=”UTF-8”>
        <HEAD>
      <link rel="stylesheet" href="../static/css/styleFormulaire.css">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
      <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
    <TITLE class="list">Gestion des créneaux </TITLE>
    </HEAD> 
    <BODY class="essai">
      <header>
        <?php
          include("headerAdmin.php");
        ?>
      </header>

      <form class="mb-3 mt-md-4" action="/del_creneau" method="POST">

      <h2 class="fw-bold mb-2">Suppression d'un créneau</h2>


      <div class="mb-3">
      <label> Numéro :  </label>
      <select id="numero" name="numero" class="form-select" required>
        <?php
        foreach($data['message'] as $creneau){
          $cren = $creneau->getNumero();
          echo "<option value=$cren>$cren</option>";
        }
        ?>
      </select>
      </div>
      <div class="suppCren">
      <button type="submit" class="btn btn-outline-dark">Valider</button>
      </div>
      </form>


      <form class="mb-3 mt-md-4" action="/update_cren" method="GET">

      <h2 class="fw-bold mb-2">Modification d'un créneau</h2>


      <div class="mb-3">
      <label> Numéro :  </label>
      <select id="numero" name="numero" class="form-select" required>
        <?php
        foreach($data['message'] as $creneau){
          $cren = $creneau->getNumero();
          echo "<option value=$cren>$cren</option>";
        }
        ?>
      </select>
      </div>
      <div class="suppCren">
      <button type="submit" class="btn btn-outline-dark">Valider</button>
      </div>
      </form>





      <div class="vh-100 d-flex justify-content-center align-items-center">
        <div class="container">
          <div class="row d-flex justify-content-center" >
            <div class="col-20 col-md-15 col-lg-15" >
              <div class="border border-3 border-danger"></div>
              <div class="card bg-white">
                <div class="card-body p-5">

                <div class="mb-3">
                    <a class="btn btn-danger" href = "creneau_add" method = "GET" role="button">Ajouter un creneau</a>
                </div>



                <table align = "center" border = "2" cellpadding = "5" cellspacing = "10">  
                <tr>  
                <td> Numéro </td>  
                <td> Jour </td>  
                <td> Horaire de début </td>  
                <td> Horaire de fin </td>  
                <td>  Division </td>
                <td> Catégorie </td>  
                <td>  Capacite </td>
                <td>  File attente </td> 
                </tr>  
                <?php 
                $x = 10;  
                $price = 35;  
                $quatity = 1;

                foreach($data['message'] as $array){
                  echo "<tr>";
                  $numero = $array->getNumero();
                  $jour = $array->getJour();
                  $horaireDeb = $array->getHoraireDeb();
                  $horaireFin = $array->getHoraireFin();
                  $divison = $array->getDiv();
                  $cat = $array->getCat();
                  $capacite = $array->getCapacite();
                  $file = $array->getFileAttente();
                  echo "<td> $numero </td>";  
                  echo "<td> $jour </td>";  
                  echo "<td> $horaireDeb </td>";  
                  echo "<td> $horaireFin </td>";  
                  echo "<td> $divison </td>";  
                  echo "<td> $cat </td>";  
                  echo "<td> $capacite </td>";
                  echo "<td> $file </td>";    
                    echo "</tr>";  
                  $x += 10;  
                  $quatity++;  
                }
                ?>    
                </table>  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </BODY>
    </HTML>
