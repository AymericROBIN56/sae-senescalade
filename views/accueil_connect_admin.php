<!DOCTYPE html>
  <meta charset=”UTF-8”>
  <HEAD>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
  <TITLE >Espace admin</TITLE>
  </HEAD>
  <BODY class="essai">
    <header>
      <?php 
        include("headerAdmin.php");
      ?>
    </header>
    <div style="text-align: center;">
        <div class="vh-100 d-flex justify-content-center align-items-center">
          <div class="container">
            <div class="row d-flex justify-content-center" >
              <div class="col-12 col-md-8 col-lg-6" >
                <div class="border border-3 border-danger"></div>
                <div class="card bg-white">
                  <div class="card-body p-5">
                    <div>
                      <h1>Bienvenue sur votre espace administrateur Senescalade </h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </BODY>
  </HTML> 