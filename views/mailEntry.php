<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
    <title>Validation mail </title>

    
    
    
    
    
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid black;
        }

        th, td {
            padding: 10px;
            text-align: center;
        }

        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <div id="app">
        <header>
            <?php
                include("header.php");
            ?>
    <form class="mb-3 mt-md-4" action="/user_add" method="GET">

        <div class="mb-3">
            <label for="creneau" class="form-label ">E-mail : </label>
            <input type="email" name="mail" v-model="mail" class="form-control" id="mail" placeholder="Entrez votre e-mail" required>
        </div>

        <div class="d-grid">
            <button type="submit" class="btn btn-outline-dark">Valider</button>
        </div>

    </form>
    </div>

</body>

</html>
