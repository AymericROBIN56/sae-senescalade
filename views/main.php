<!DOCTYPE html>
  <meta charset=”UTF-8”>
  <meta name=“keywords” content=”BUT, Vannes, Web”>
  <HEAD>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">

    <TITLE >Senescalade</TITLE>
  </HEAD>
  <body class="essai">

  <header>
      <?php
      include("header.php")
      ?>
      </header>
    <div style="text-align: center;" id="app">

        <a class="navig" @click="cours">Cours et catégorie </a>
        <a class="navig" href="https://www.ffme.fr/escalade/formation-escalade/passeports/">Passeport FFME</a>
        <div class="vh-100 d-flex justify-content-center align-items-center" v-if="showMain">
          <div class="container">
            <div class="row d-flex justify-content-center" >
              <div class="col-12 col-md-8 col-lg-6" >
                <div class="border border-3 border-danger "></div>
                <div class="card bg-white">
                  <div class="card-body p-5">
                    <div>
                      <h1>Senescalade</h1>
                      <p>Bonjour et bienvenue sur le site de Senescalade </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


    <div v-if="showCours == true" style="text-align : center;" class="infos">
      <div>
        <a class="navig" @click="toAdultes">Adultes</a>
        <a class="navig" @click="toJeunes">Jeunes</a>
        <a class="navig" @click="toCompet">Compétition</a>
        <a class="navig" @click="toBaby">BabyGrimpe</a>
      </div>

      <div v-if="Adultes">
        <div class="infos">
          <h2 style="margin-bottom:20px;">Horaires Adultes : </h2>
          <div class="tab">
          <table class="hor" align="center" border="1">
            <tr>
              <th style="color:red;">Jour</th>
              <th style="color:red;">Horaires</th>
              <th style="color:red;">Informations</th>
            </tr>
            <tr>
              <td>Lundi</th>
              <td>19H - 21H</td>
              <td>Séance encadrée</td>
            </tr>
            <tr>
              <td>Mardi</th>
              <td>20H - 22H</td>
              <td>Séance encadrée</td>
            </tr><tr>
              <td>Mercredi</th>
              <td>19H30 - 21H30</td>
              <td>Séance encadrée</td>
            </tr><tr>
              <td>Jeudi</th>
              <td>19H - 21H</td>
              <td>Grimpeurs autonomes</td>
            </tr><tr>
              <td>Vendredi</th>
              <td>19H30 - 21H30</td>
              <td>Séance encadrée</td>
            </tr>
          </table>
          <h3 style="margin-top:20px;">Prix: </h3>
            <h3> 140 € / an</h3>
        </div>
        <div>
          <h2>Combien de fois puis-je grimper par semaine ? </h2>
          <p style="color : red;">De septembre à la Toussaint :</p>
          <p>une séance par semaine</p>
          <p style="color :red">De la Toussaint à la fin d'année :</p> 
          <p>Possibilité de venir 2 fois par semiane</p>
          <p style="color :red;">A partir de janvier :</p> 
          <p>on vient quand on veut à la condition d'avoir le passeport jaune pour les séances autonomes</p>
        </div>
        </div>
      </div>





      <div v-if="Jeunes">
      <div class="infos">
          <h2 style="margin-bottom:20px;">Horaires Jeunes : </h2>
          <table class="hor" align="center">
            <tr>
              <th style="color:red;">Jour</th>
              <th style="color:red;">Horaires</th>
              <th style="color:red;">Catégorie</th>
            </tr>
            <tr>
              <td>Lundi</th>
              <td>17H30 - 19H</td>
              <td>U14</td>
            </tr>
            <tr>
              <td>Mardi</th>
              <td>17H30 - 19H</td>
              <td>U12</td>
            </tr>
            <tr>
              <td>Mardi</th>
              <td>19H - 20H30</td>
              <td>Perf</td>
            </tr>
            <tr>
              <td>Mercredi</th>
              <td>15H30 - 17H</td>
              <td>U10</td>
            </tr>
            <tr>
              <td>Jeudi</th>
              <td>17H30 - 19H</td>
              <td>U16/U18</td>
            </tr>
            <tr>
              <td>Samedi</th>
              <td>9H30 - 11H</td>
              <td>U10</td>
            </tr>
            <tr>
              <td>Samedi</th>
              <td>11H - 12H30</td>
              <td>U12</td>
            </tr>
            <tr>
              <td>Samedi</th>
              <td>13H30 - 15H</td>
              <td>U12</td>
            </tr>
            <tr>
              <td>Samedi</th>
              <td>15H - 16H30</td>
              <td>U14</td>
            </tr>
            <tr>
              <td>Samedi</th>
              <td>16H30 - 18H</td>
              <td>U16</td>
            </tr>
          </table>
          <h3 style="margin-top:20px;">Prix: </h3>
            <h3> 140 € / an</h3>
        </div>
      </div>
      <div v-if="Compet">
      <div class="infos">
          <h2 style="margin-bottom:20px;">Horaires Compétitions : </h2>
          <table class="hor" align="center">
            <tr>
              <th style="color:red;">Jour</th>
              <th style="color:red;">Horaires</th>
              <th style="color:red;">Informations</th>
            </tr>
            <tr>
              <td>Mercredi</th>
              <td>17H - 19H30</td>
              <td>Entraînement encadré</td>
            </tr>
            <tr>
              <td>Vendredi</th>
              <td>17H30 - 19H30</td>
              <td>Entraînement encadré</td>
            </tr>
          </table>
          <h3 style="margin-top:20px;">Prix: </h3>
            <h3> 180 € / an</h3>
        </div>
      </div>
      <div v-if="Baby">
      <div class="infos">
          <h2 style="margin-bottom:20px;">Horaires BabyGrimpe : </h2>
          <table class="hor" align="center">
            <tr>
              <th style="color:red;">Jour</th>
              <th style="color:red;">Horaires</th>
              <th style="color:red;">Informations</th>
            </tr>
            <tr>
              <td>Dimanche</th>
              <td>10H - 12H</td>
              <td>Séance encadré par les parents</td>
            </tr>
          </table>
          <h3 style="margin-top:20px;">Prix: </h3>
            <h3> 95 € / an</h3>
        </div>
      </div>
    </div>
    </div>
  




    <footer>
      <?php
        include("form_contact.php");
      ?>
    </footer>


    <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
    <script defer src="../js/main.js"></script>

</body>
</html> 
