<!DOCTYPE HTML>
<HTML>
    <meta charset=”UTF-8”>
        <HEAD>
      <link rel="stylesheet" href="../static/css/styleFormulaire.css">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
      <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
    <TITLE class="list">Gestion des administrateurs </TITLE>
    </HEAD> 
    <BODY class="essai">
      <header>
        <?php
          include("headerAdmin.php");
        ?>
      </header>
          <form class="mb-3 mt-md-4" action="/del_admin" method="POST">
          <h2 class="fw-bold mb-2">Suppression administrateur</h2>


          <div class="mb-3">
            <label> Prénom :  </label>
            <select name="prenom" id="prenom" class="form-select" required style="margin-bottom:20px;">
              <?php
              foreach($data['message'] as $admin){
                $prenom = $admin->getPrenom();
                echo "<option value=$prenom>$prenom</option>";
              }
              ?>
            </select>

            <label> Nom :  </label>
            <select name="nom" id="nom" class="form-select" required>
              <?php
              foreach($data['message'] as $admin){
                $nom = $admin->getNom();
                echo "<option value=$nom>$nom</option>";
              }
              ?>
            </select>

          </div>

          <div class="suppCren">
            <button type="submit" class="btn btn-outline-dark">Valider</button>
          </div>
          </form>
      <div class="vh-100 d-flex justify-content-center align-items-center">
        <div class="container">
          <div class="row d-flex justify-content-center" >
            <div class="col-20 col-md-15 col-lg-15" >
              <div class="border border-3 border-danger"></div>
              <div class="card bg-white">
                <div class="card-body p-5">

                <div class="mb-3">
                    <a class="btn btn-danger" href = "/add_admin" method = "GET" role="button">Ajouter un administrateur</a>
                </div>



                <table align = "center" border = "1" cellpadding = "3" cellspacing = "2">  
                <tr>  
                <td> Nom </td>  
                <td> Prenom </td>  
                <td> Mail </td> 
                <td>  Telephone </td>
                <td>  Mobile </td> 
                </tr>  
                <?php 
                $x = 10;  
                $price = 35;  
                $quatity = 1;

                foreach($data['message'] as $array){
                  echo "<tr>";
                  $nom = $array->getNom();
                  $prenom = $array->getPrenom();
                  $mail = $array->getMail();
                  $tel = $array->getTel();
                  $mobile = $array->getMobile();
                  echo "<td> $nom </td>";  
                  echo "<td> $prenom </td>";  
                  echo "<td> $mail </td>";  
                  echo "<td> $tel </td>";  
                  echo "<td> $mobile </td>";  
                    echo "</tr>";  
                  $x += 10;  
                  $quatity++;  
                }
                ?>    
                </table>  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </BODY>
    </HTML>