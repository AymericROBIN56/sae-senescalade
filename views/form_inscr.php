<!DOCTYPE html>
<meta charset=”UTF-8”>
<HEAD>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
  <TITLE class="list">Inscription</TITLE>
</HEAD>
<body id="app">
  <header>
    <?php
      include('header.php');
    ?>
   <h2 id="regi">Enregistrement : </h2>
  <form class="mb-3 mt-md-4" action="/choix" method="GET">


  <div class="mb-3">
  <label for="action" class="form-label"> Action : </label>
    <input type="text" name="action" class="form-control" id="action" value= <?php echo $data['action'] ?> readonly>
  </div>

  <div class="mb-3">
    <label for="nom" class="form-label">Nom de Famille *  : </label>
    <input type="text" name="nom" class="form-control" id="nom" placeholder="Nom de famille" required>
  </div>
  <div class="mb-3">
    <label for="prenom" class="form-label">Prenom * : </label>
      <input type="text" name="prenom" class="form-control" id="prenom" placeholder="Prenom" required >
  </div>
  <div class="mb-3">
    <label for="date" class="form-label">Date de Naissance * : </label>
    <input type="date" name="date" class="form-control" id="date" placeholder="jj/mm/aaaa" required>
  </div>
  
  <div class="mb-3" >
    <label for="numLicence" class="form-label ">Sexe *  : </label>
    <select class="form-select" id="sexe" name="sexe">
      <option value="H">Homme</option>
      <option value="F">Femme</option>
    </select>
  </div>

  <div class="mb-3">
    <label for="nationalite" class="form-label ">Nationalite * : </label>
    <input type="text" name="nationalite" class="form-control" id="nationalite" placeholder= "France = FR" value="FR"  required>
  </div>



  <div class="mb-3">
  <label for="adresse" class="form-label"> Adresse *  : </label>
    <input type="text" name="adresse" class="form-control" placeholder=" Adresse  " required>
  </div>
  <div class="mb-3">
    <label for ="complAdr" class="form-label"> Complément adresse : </label>
    <input type="text" name="complAdr" class="form-control" placeholder="Etages, batiment, numéro ... " >
  </div>
  <div class="mb-3">
    <label for="codePostal" class="form-label ">Code Postal * : </label>
    <input type="number" name="codePostal" class="form-control" id="codePostal" placeholder= "codePostal"  required>
  </div>
  <div class="mb-3">
    <label for="ville" class="form-label ">Ville * : </label>
    <input type="text" name="ville" class="form-control" id="ville" placeholder= "Ville"  required>
  </div>
  <div class="mb-3">
    <label for="pays" class="form-label ">Pays * : </label>
    <input type="text" name="pays" class="form-control" id="pays" placeholder= "France = FR" value="FR"  required>
  </div>
  <div class="mb-3">
    <label for="tel" class="form-label ">Téléphone : </label>
    <input type="text" name="tel" class="form-control" id="tel" placeholder= "" >
  </div>
  <div class="mb-3">
    <label for="mobile" class="form-label ">Mobile : </label>
    <input type="text" name="mobile" class="form-control" id="mobile" placeholder= "" >
  </div>
  <div class="mb-3">
    <label for="mail" class="form-label ">Mail : </label>
    <input type="email" name="mail" class="form-control" id="mail" value =<?php echo $data['mail'] ?>  readonly>
  </div>
  <div class="mb-3">
    <label for="mail" class="form-label ">Mot de passe *  : </label>
    <input type="password" name="mdp" class="form-control" id="mdp" placeholder= "Minimum 7 caractères"  required>
  </div>
  <div class="mb-3">
    <label for="prevNom" class="form-label ">Personne à prévénir- Nom : </label>
    <input type="text" name="prevNom" class="form-control" id="prevNom" placeholder= "Nom">
  </div>
  <div class="mb-3">
    <label for="prevPrenom" class="form-label ">Personne à prévénir- Prénom : </label>
    <input type="text" name="prevPrenom" class="form-control" id="prevPrenom" placeholder= "Prenom">
  </div>
  <div class="mb-3">
    <label for="prevTel" class="form-label ">Personne à prévénir- Téléphone : </label>
    <input type="text" name="prevTel" class="form-control" id="prevTel" placeholder= "Telephone">
  </div>
  <div class="mb-3">
    <label for="prevMail" class="form-label ">Personne à prévénir- Mail : </label>
    <input type="text" name="prevMail" class="form-control" id="prevMail" placeholder= "Adresse Mail">
  </div>
  <div class="mb-3">
  <?php
    if ($data['action'] == "Renouvellement"){
      echo "<p> 6 chiffres pour le numéro de licence </p>";
      echo "<label for=numLicence class=form-label >Numéro de licence * : </label>";
      echo "<input type=text name=numLicence class=form-control id=numLicence  required>";
    }
    ?>
  </div>
  

  <div class="mb-3" >
    <label for="numLicence" class="form-label ">Type de licence *  : </label>

    <select class="form-select" id="typeLicence" name="typeLicence" required>
        <option value="J">Jeune</option>
        <option value="A">Adulte</option>
        <option value="F">Famille</option>
    </select>
  </div>

  <p> Types assurances : RC = Responsabilité civile , B = Base, B+ = Base + et B++ = Base ++ </p>

  <div class="mb-3" >
    <label for="typeAssurance" class="form-label "> Assurance *  : </label>
    <select class="form-select" name="typeAssurance" id="typeAssurance" required>
        <option value="B">B</option>
        <option value="RC">RC</option>
        <option value="B+">B+</option>
        <option value="B++">B++</option>
    </select>
  </div>
  
  <div class="mb-3">
    <label for="ski" class="form-label ">Option Ski *  : </label>
    <select class="form-select" id="ski" name="ski" required>
      <option value="NON">Non</option>
      <option value="OUI">Oui</option>
    </select>
  </div>

  <div class="mb-3">
    <label for="slackline" class="form-label ">Option Slackline *  : </label>
    <select class="form-select" id="slackline" name="slackline" required>
      <option value="NON">Non</option>
      <option value="OUI">Oui</option>
      </select>
  </div>

  <div class="mb-3">
    <label for="trail" class="form-label "> Option Trail *  : </label>
    <select class="form-select" id="trail" name="trail" required>
      <option value="NON">Non</option>
      <option value="OUI">Oui</option>
    </select>
  </div>

  <div class="mb-3">
    <label for="vtt" class="form-label ">Option VTT * : </label>
    <select class="form-select" id="vtt" name="vtt" required>
      <option value="NON">Non</option>
      <option value="OUI">Oui</option>
      </select>
  </div>

  <div class="mb-3" >
    <label for="numLicence" class="form-label ">Option assurance *  : </label>
    <select class="form-select" id="optAssur" name="optAssur" required>
        <option value="IJ1">IJ1</option>
        <option value="IJ2">IJ2</option>
        <option value="IJ3">IJ3</option>
        <option value="NON">Non</option>
    </select>
  </div>

  <div class="d-grid">
    <button type="submit" class="btn btn-outline-dark" >Création du compte</button>
  </div>
  </form>
</body>
</HTML>