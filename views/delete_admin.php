<!DOCTYPE html>
<meta charset=”UTF-8”>
<HEAD>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
  <TITLE class="list">Suppression administrateur</TITLE>
</HEAD>
<body>
  <header>
    <?php
      include("headerAdmin.php");
    ?>
  </header>

  <form class="mb-3 mt-md-4" action="/del_admin" method="POST">
  <h2 class="fw-bold mb-2">Suppression administrateur</h2>


  <div class="mb-3">
    <label> Prénom :  </label>
    <select name="prenom" id="prenom" class="form-select" required style="margin-bottom:20px;">
      <?php
      foreach($data['tab'] as $admin){
        $prenom = $admin->getPrenom();
        echo "<option value=$prenom>$prenom</option>";
      }
      ?>
    </select>

    <label> Nom :  </label>
    <select name="nom" id="nom" class="form-select" required>
      <?php
      foreach($data['tab'] as $admin){
        $nom = $admin->getNom();
        echo "<option value=$nom>$nom</option>";
      }
      ?>
    </select>

  </div>

  <div class="suppCren">
    <button type="submit" class="btn btn-outline-dark">Valider</button>
  </div>
  </form>
</body>
</HTML>