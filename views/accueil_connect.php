<!DOCTYPE html>
  <meta charset=”UTF-8”>
  <HEAD>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
  <TITLE >Espace client</TITLE>
  <style>
        .container {
            text-align: center;
        }
        iframe {
            width: 100%;
            height: 750px;
            border: none;
            display: none; /* Initialement caché */
        }
    </style>
  </HEAD>
  
  <BODY class="essai">
    <header>
      <?php
        include("headerClient.php");
      ?>
      </header>
    <div style="text-align: center;">
        <div class="vh-100 d-flex justify-content-center align-items-center">
          <div class="container">
            <div class="row d-flex justify-content-center" >
              <div class="col-12 col-md-8 col-lg-6" >
                <div class="border border-3 border-danger"></div>
                <div class="card bg-white">
                  <div class="card-body p-5">
                    <div>
                      <h1>Bienvenue sur votre espace client Senescalade </h1>
                    </div>
                    <button id="toggleButton" class="btn btn-danger">Accéder à la boutique</button>
                    <iframe id="haWidget" allowtransparency="true" scrolling="auto" src="https://www.helloasso.com/associations/senescalade/boutiques/paiements-en-ligne/widget"></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <script>
        document.getElementById('toggleButton').addEventListener('click', function() {
            var iframe = document.getElementById('haWidget');
            if (iframe.style.display === 'none' || iframe.style.display === '') {
                iframe.style.display = 'block';
                this.textContent = 'Cacher la boutique';
            } else {
                iframe.style.display = 'none';
                this.textContent = 'Accéder à la boutique';
            }
        });
    </script>
  </BODY>
  </HTML> 