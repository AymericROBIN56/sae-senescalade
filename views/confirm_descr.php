<!DOCTYPE html>
  <meta charset=”UTF-8”>
  <HEAD>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
  <TITLE >Confirmation de désincription </TITLE>
  </HEAD>
  <BODY class="essai">
    <header>
      <?php
        include("headerClient.php");
        $datas = $data['st'][0];
      ?>
    </header>
    <div style="text-align: center;">
        <div class="vh-100 d-flex justify-content-center align-items-center">
          <div class="container">
            <div class="row d-flex justify-content-center" >
              <div class="col-12 col-md-8 col-lg-6" >
                <div class="border border-3 border-danger"></div>
                <div class="card bg-white">
                  <div class="card-body p-5">
                    <div>
                      <p>Etes-vous sûr de vouloir vous désinscrire de ce créneau : </p>
                      <form class="mb-3 mt-md-4" action="/confirm_desinscr" method="POST">
                      <p> Numéro : </p>
                      <input type="text" name="numero" class="form-control" id="numero" value= <?php echo $datas->getNumero() ?> readonly>
                      <p> Jour : </p>
                      <input type="text" name="jour" class="form-control" id="jour" value= <?php echo $datas->getJour() ?> readonly>
                      <p> Horaires : </p>
                      <input type="text" name="horaire" class="form-control" id="horaire" value= <?php echo $datas->getHoraireDeb() ?>-<?php echo $datas->getHoraireFin()?> readonly>
                      <p> Division : </p>
                      <input type="text" name="division" class="form-control" id="division" value= <?php echo $datas->getDiv() ?> readonly>

                    <div class="d-grid">
                        <button type="submit" class="btn btn-outline-dark">Validation</button>
                    </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </BODY>
  </HTML> 