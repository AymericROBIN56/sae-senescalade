<!DOCTYPE html>
  <meta charset=”UTF-8”>
  <HEAD>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
  <TITLE >Espace admin</TITLE>
  </HEAD>
  <BODY class="essai">
    <div style="text-align: center;">
        <div class="vh-100 d-flex justify-content-center align-items-center">
          <div class="container">
            <div class="row d-flex justify-content-center" >
              <div class="col-12 col-md-8 col-lg-6" >
                <div class="border border-3 border-primary"></div>
                <div class="card bg-white">
                  <div class="card-body p-5">
                    <div>
                      <h1>Bienvenue sur votre espace administrateur Senescalade </h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="position-absolute top-0 end-0">
      <a class="btn btn-primary" href = "manage_creneau" method = "GET" role="button">Gérer les créneaux</a>
      <a class="btn btn-primary" href="/disconnect" method="GET" role="button">Se déconnecter</a>
    </div>

    <div class="position-absolute top-0 end-25">
      <a class="btn btn-primary" href = "manage_admin" method = "GET" role="button">Gérer les administrateurs</a>
      <a class="btn btn-primary" href = "manage_user" method = "GET" role="button">Gérer les utilisateurs</a>
    </div>

  </BODY>
  </HTML> 