<!DOCTYPE html>
    <meta charset=”UTF-8”>
    <HEAD>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
      <TITLE class="list"> Ajout Creneaux</TITLE>
    </HEAD>
    <BODY class="essai">
      <header>
        <?php
          include("headerAdmin.php");
        ?>
      </header>
      <div class="vh-100 d-flex justify-content-center align-items-center">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
              <div class="border border-3 border-danger"></div>
              <div class="card bg-white">
                <div class="card-body p-5">
                  <form class="mb-3 mt-md-4" action='/creneau_add' method = "POST">
                    <h2 class="fw-bold mb-2">Senescalade</h2>
                    <div class="mb-3">
                                    <label for="jour" class="form-label">Jour : </label>
                                    <select name="jour" class="form-select" id="jour" required>
                                        <option value="Lundi">Lundi</option>
                                        <option value="Mardi">Mardi</option>
                                        <option value="Mercredi">Mercredi</option>
                                        <option value="Jeudi">Jeudi</option>
                                        <option value="Vendredi">Vendredi</option>
                                        <option value="Samedi">Samedi</option>
                                        <option value="Dimanche">Dimanche</option>
                                        
                                    </select>
                    </div>

                    <div class="mb-3">
                      <label for="horaireDeb" class="form-label "> Horaire de début : </label>
                      <input type="text" name="horaireDeb" class="form-control" id="horaireDeb" placeholder="hhHmm" required>
                    </div>


                    <div class="mb-3">
                      <label for="horaireFin" class="form-label "> Horaire de fin : </label>
                      <input type="text" name="horaireFin" class="form-control" id="horaireFin" placeholder="hhHmm" required>
                    </div>

                    <div class="mb-3">
                                    <label for="division" class="form-label"> Division : </label>
                                    <select name="division" class="form-select" id="division" required>
                                        <option value="U8">U8</option>
                                        <option value="U10">U10</option>
                                        <option value="U12">U12</option>
                                        <option value="U14">U14</option>
                                        <option value="U16">U16</option>
                                        <option value="U18">U18</option>
                                        <option value="U20">U20</option>
                                        <option value="+20">+20</option>
                                    </select>
                    </div>

                    <div class="mb-3">
                      <label for="cat" class="form-label">Catégorie : </label>
                        <select id="cat" name="cat" class="form-select" required>
                          <option value="Adultes">Adultes</option>
                          <option value="Jeunes">Jeunes</option>
                          <option value="Compétition">Compétition</option>
                          <option value="Babygrimpe">Babygrimpe</option>  
                        </select>
                      </div>

                    <div class="mb-3">
                      <label for="prix" class="form-label">Prix : </label>
                      <input type="number" id="prix" name="prix" required>
                    </div>

                    <div class="mb-3">
                      <label for="descr" class="form-label">Description : </label>
                      <input type="text" id="descr" name="descr">
                    </div>


                    <div class="mb-3">
                      <label for="horaires" class="form-label "> Capacité de la séance : </label>
                      <input type="text" name="capacite" class="form-control" id="capacite" placeholder="nombre de personnes maximum" required>
                    </div>
                    

                    <div class="d-grid">
                    <button type="submit" class="btn btn-outline-dark">Valider</button>
                    </div>
                  </form>
      
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </BODY>
    </HTML>