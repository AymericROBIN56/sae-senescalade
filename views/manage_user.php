<!DOCTYPE HTML>
<HTML>
    <meta charset=”UTF-8”>
        <HEAD>
      <link rel="stylesheet" href="../static/css/styleFormulaire.css">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
      <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
    <TITLE class="list">Gestion des utilisateurs </TITLE>
    </HEAD> 
    <BODY class="essai">
    <header>
      <?php
        include("headerAdmin.php");
      ?>
    </header>
    <script>
    function downloadCSV() {
        // Appeler le script PHP qui génère le fichier CSV
        window.location.href = '/model/TelechargementCSV.php';
    }
	</script>

      <form class="mb-3 mt-md-4" action="/del_user" method="POST">
        <h2 class="fw-bold mb-2">Suppression Utilisateur</h2>


        <div class="mb-3">
          <label> Numéro utilisateur :  </label>
          <select class="form-select" name="id" id="id" required>
            <?php
              foreach($data['message'] as $use){
                $user = $use->getId();
                echo "<option value=$user>$user</option>";
              }
            ?>
            </select>
        </div>
        <div class="suppCren">
          <button type="submit" class="btn btn-outline-dark">Valider</button>
        </div>
        </form>
        
      <div class="vh-100 d-flex justify-content-center align-items-center">
        <div class="container">
          <div class="row d-flex justify-content-center" >
            <div class="col-20 col-md-15 col-lg-15" >
              <div class="border border-3 border-danger"></div>
              <div class="card bg-white">
                <div class="card-body p-5">


                <div class="mb-3">
                    <a class="btn btn-success" id="dl" onclick="downloadCSV()">Télécharger</a>
                </div>


                <table align = "center" border = "1" cellpadding = "4" cellspacing = "5">  
                <tr>  
                <td> Numéro utilisateur </td>
                <td> Nom </td>  
                <td> Prenom </td>  
                <td> Mail </td>  
                <td>  Adresse </td>
                <td> Complément adresse </td>
                <td> Date de naissance </td>  
                <td> Action </td>
                <td>  Code postal </td>
                <td>  Telephone </td>
                <td>  Mobile </td> 
                <td>  Sexe </td>
                <td>  Nationalite </td> 
                <td>  Ville </td>
                <td>  Pays </td>
                </tr>
                <?php 
                $x = 10;  
                $price = 35;  
                $quatity = 1;

                foreach($data['message'] as $array){
                  echo "<tr>";
                  $num = $array->getId();
                  $nom = $array->getNom();
                  $prenom = $array->getPrenom();
                  $mail = $array->getMail();
                  $adresse = $array->getAdresse();
                  $complAdr = $array->getComplAdr();
                  $date = $array->getDate();
                  $action = $array->getAction();
                  $codePostal = $array->getCodePostal();
                  $tel = $array->getTel();
                  $mobile = $array->getMobile();
                  $sexe = $array->getSexe();
                  $nat = $array->getNationalite();
                  $ville = $array->getVille();
                  $pays = $array->getPays();
                  echo "<td> $num </td>";
                  echo "<td> $nom </td>";  
                  echo "<td> $prenom </td>";  
                  echo "<td> $mail </td>";  
                  echo "<td> $adresse </td>";
                  echo "<td> $complAdr </td>";
                  echo "<td> $date </td>";
                  echo "<td> $action </td>";  
                  echo "<td> $codePostal </td>";
                  echo "<td> $tel </td>";  
                  echo "<td> $mobile </td>";
                  echo "<td> $sexe </td>";
                  echo "<td> $nat </td>";
                  echo "<td> $ville </td>";
                  echo "<td> $pays </td>";
                    echo "</tr>";  
                  $x += 10;  
                  $quatity++;  
                }
                ?>
                <script>
    function downloadCSV() {
        // Contenu du fichier CSV
        var csvContent = "Action,Nom,Prénom,Date naissance,Sexe,Nationalité,Adresse,Complément d'adresse,Code postal,Ville,Pays,Téléphone,Mobile,Courriel,Courriel2,Personne à prévenir - Nom,Personne à prévenir - Prenom,Personne à prévenir - Téléphone,Personne à prévenir - Courriel,N°licence,Type licence,Assurance,Option ski,Option slackline,Option trail,Option VTT,Option Assurance,\n";

        <?php
        foreach ($data['message'] as $array) {
            $nom = $array->getNom();
          $prenom = $array->getPrenom();
          $mail = $array->getMail();
          $adresse = $array->getAdresse();
          $complAdr = $array->getComplAdr();
          $date = $array->getDate();
          $action = $array->getAction();
          $codePostal = $array->getCodePostal();
          $tel = $array->getTel();
          $mobile = $array->getMobile();
          $sexe = $array->getSexe();
          $nat = $array->getNationalite();
          $ville = $array->getVille();
          $pays = $array->getPays();
          $prevNom = $array->getPrevNom();
          $prevPrenom = $array->getPrevPrenom();
          $prevTel = $array->getPrevTel();
          $prevMail = $array->getPrevMail();
          $num = $array->getNumeroLicence();
          $type = $array->getTypeLicence();
          $assurance = $array->getAssurance();
          $ski = $array->getSki();
          $slackline = $array->getSlackline();
          $trail = $array->getTrail();
          $vtt = $array->getVTT();
          $opAssur = $array->getOpAssurance();

            // Ajout des valeurs au contenu CSV en JavaScript
            echo "csvContent += '$action,$nom,$prenom,$date,$sexe,$nat,$adresse,$complAdr,$codePostal,$ville,$pays,$tel,$mobile,$mail,$mail,$prevNom,$prevPrenom,$prevTel,$prevMail,$num,$type,$assurance,$ski,$slackline,$trail,$vtt,$opAssur,\\n';\n";
        }
        ?>

        // Création d'un Blob avec le contenu CSV
        var blob = new Blob([csvContent], { type: 'text/csv' });

        // Création d'un objet URL à partir du Blob
        var url = window.URL.createObjectURL(blob);

        // Création d'un élément d'ancre (lien) pour le téléchargement
        var a = document.createElement("a");
        a.href = url;
        a.download = "donnees.csv";

        // Ajout de l'élément ancre au document
        document.body.appendChild(a);

        // Simulation d'un clic sur le lien pour déclencher le téléchargement
        a.click();

        // Suppression de l'élément ancre du document
        document.body.removeChild(a);
    }
</script> 
                </table>  
                </div>
              </div>
            </div>
          </div>

          <div class="col-20 col-md-15 col-lg-15" >
              <div class="card bg-white">
                <div class="card-body p-5">


                


                <table align = "center" border = "1" cellpadding = "3" cellspacing = "5">  
                <tr>  
                <td>  Personne à prevenir - Nom </td>
                <td>  Personne à prevenir - Prénom </td>
                <td>  Personne à prevenir - Téléphone </td>
                <td>  Personne à prevenir - Mail </td>
                <td>  Numéro de licence </td>
                <td>  Type de licence </td>
                <td>  Assurance </td>
                <td>  Ski </td>
                <td>  Slackline </td>
                <td>  Trail </td>
                <td>  Vtt </td>
                <td>  Option assurance </td>
                <td> Statut </td>
                </tr>  
                <?php 
                $x = 10;  
                $price = 35;  
                $quatity = 1;

                foreach($data['message'] as $array){
                  echo "<tr>";
                  $prevNom = $array->getPrevNom();
                  $prevPrenom = $array->getPrevPrenom();
                  $prevTel = $array->getPrevTel();
                  $prevMail = $array->getPrevMail();
                  $num = $array->getNumeroLicence();
                  $type = $array->getTypeLicence();
                  $assurance = $array->getAssurance();
                  $ski = $array->getSki();
                  $slackline = $array->getSlackline();
                  $trail = $array->getTrail();
                  $vtt = $array->getVTT();
                  $opAssur = $array->getOpAssurance();
                  $statut = $array->getStatut();
                  echo "<td> $prevNom </td>";  
                  echo "<td> $prevPrenom </td>";  
                  echo "<td> $prevTel </td>";  
                  echo "<td> $prevMail </td>";  
                  echo "<td> $num </td>";
                  echo "<td> $type </td>";  
                  echo "<td> $assurance </td>";
                  echo "<td> $ski </td>";
                  echo "<td> $slackline </td>";
                  echo "<td> $trail </td>";
                  echo "<td> $vtt </td>";  
                  echo "<td> $opAssur </td>";
                  echo "<td> $statut </td>";
                    echo "</tr>";  
                  $x += 10;  
                  $quatity++;  
                }
                ?>    
                </table>  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </BODY>
    </HTML>
