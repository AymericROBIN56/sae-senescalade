<!DOCTYPE htmlL>
<meta charset=”UTF-8”>
<HEAD>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
  <TITLE class="list">Modification profil</TITLE>
</HEAD>
<body>
  <header>
    <?php
      include("headerClient.php");
    ?>
  </header>

  <form class="mb-3 mt-md-4" action="/update" method="POST">
  <h2 class="fw-bold mb-2">Modification informations : </h2>
  
  <div class = "mb-3">
    <label for="nationalite" class="form-label">Nationalite : </label>
    <input type="text" name ="nationalite" class="form-control" id="nationalite" placeholder="France = FR" value= <?php echo $data['nat'] ?> required>
  </div>
  <div class="mb-3">
  <label for="adresse" class="form-label"> Adresse : </label>
    <input type="text" name="adresse" class="form-control" placeholder=" Adresse " value=<?php echo $data['adresse'] ?> required>
  </div>
  <div class="mb-3">
    <label for="complAdr" class="form-label"> Complément adresse : </label>
    <input type="text" name="complAdr" class="form-control" placeholder="Etages, batiment, numéro ... " >
  </div>
  <div class="mb-3">
    <label for="codePostal" class="form-label ">Code Postal : </label>
    <input type="number" name="codePostal" class="form-control" id="codePostal" placeholder= "codePostal" value=<?php echo $data['codePostal'] ?> required>
  </div>
  <div class="mb-3">
    <label for="ville" class="form-label ">Ville : </label>
    <input type="text" name="ville" class="form-control" id="ville" placeholder= "Ville" value=<?php echo $data['ville'] ?> required>
  </div>
  <div class="mb-3">
    <label for="pays" class="form-label ">Pays : </label>
    <input type="text" name="pays" class="form-control" id="pays" placeholder= "France = FR" value=<?php echo $data['pays'] ?> required>
  </div>
  <div class="mb-3">
    <label for="telephone" class="form-label ">Téléphone : </label>
    <input type="text" name="tel" class="form-control" id="tel">
  </div>
  <div class="mb-3">
    <label for="mobile" class="form-label ">Mobile : </label>
    <input type="text" name="mobile" class="form-control" id="mobile">
  </div>
  <div class="mb-3">
    <label for="mail" class="form-label ">Mail : </label>
    <input type="email" name="mail" class="form-control" id="mail" placeholder= "Ex : adresse.mail@gmail.com" value=<?php echo $data['mail'] ?> required>
  </div>
  <div class="mb-3">
    <label for="mail" class="form-label ">Mot de passe : </label>
    <input type="password" name="mdp" class="form-control" id="mdp" placeholder= "Minimum 7 caractères" value=<?php echo $data['mdp'] ?> required>
  </div>
  <div class="mb-3">
    <label for="prevNom" class="form-label ">Personne à prévénir- Nom : </label>
    <input type="text" name="prevNom" class="form-control" id="prevNom" placeholder= "Nom" >
  </div>
  <div class="mb-3">
    <label for="prevPrenom" class="form-label ">Personne à prévénir- Prénom : </label>
    <input type="text" name="prevPrenom" class="form-control" id="prevPrenom" placeholder= "Prenom" >
  </div>
  <div class="mb-3">
    <label for="prevTel" class="form-label ">Personne à prévénir- Téléphone : </label>
    <input type="text" name="prevTel" class="form-control" id="prevTel" placeholder= "Telephone" >
  </div>
  <div class="mb-3">
    <label for="prevMail" class="form-label ">Personne à prévénir- Mail : </label>
    <input type="text" name="prevMail" class="form-control" id="prevMail" placeholder= "Adresse Mail" >
  </div>

  <p> Types assurances : RC = Responsabilité civile , B = Base, B+ = Base + et B++ = Base ++ </p>

  <div class="mb-3" >
    <label for="numLicence" class="form-label "> Assurance   : </label>
    <select id="assur" name="assur" class="form-select">
      <option value="B">B</option>
      <option value="B+">B+</option>
      <option value="B++">B++</option>
      <option value="RC">RC</option>
    </select>
  </div>
  
  <div class="mb-3">
    <label for="numLicence" class="form-label ">Option Ski : </label>
    <select id="ski" name="ski" class="form-select">
      <option value="NON">NON</option>
      <option value="OUI">OUI</option>
    </select>
  </div>

  <div class="mb-3">
    <label for="numLicence" class="form-label ">Option Slackline : </label>
    <select id="slackine" name="slackline" class="form-select">
      <option value="NON">NON</option>
      <option value="OUI">OUI</option>
    </select>
  </div>

  <div class="mb-3">
    <label for="numLicence" class="form-label "> Option Trail : </label>
    <select id="trail" name="trail" class="form-select">
      <option value="NON">NON</option>
      <option value="OUI">OUI</option>
    </select>
  </div>

  <div class="mb-3">
    <label for="numLicence" class="form-label ">Option VTT : </label>
    <select id="vtt" name="vtt" class="form-select">
      <option value="NON">NON</option>
      <option value="OUI">OUI</option>
    </select>
  </div>

  <div class="mb-3" >
    <label for="numLicence" class="form-label ">Option assurance : </label>
    <select id="opAssur" name="opAssur" class="select">
      <option value="IJ1">IJ1</option>
      <option value="IJ2">IJ2</option>
      <option value="IJ3">IJ3</option>
      <option value="NON">NON</option>
    </select>
  </div>



  <div class="d-grid">
    <button type="submit" class="btn btn-outline-dark">Valider la modification</button>
  </div>
  </form>
</body>
</HTML>
