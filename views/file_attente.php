<!DOCTYPE html>
<head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="../static/css/styleFormulaire.css">
  <TITLE class="File attente"></TITLE>
</head>
  <body class="essai">
    <header>
      <?php
      include("header.php");
      ?>
    </header>
    <div class="vh-100 d-flex justify-content-center align-items-center">
      <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-12 col-md-8 col-lg-6">
            <div class="border border-3 border-danger"></div>
              <div class="card bg-white">
                <div class="card-body p-5">
                  <h2 class="fw-bold mb-2">File attente :  vous êtes le numéro <?php echo $data['message']; ?> sur la file attente du créneau <?php echo $data['creneau'];?> </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="position-absolute top-0 end-0">
      <a class="btn btn-primary" href="/" role="button">Accueil</a>
      <a class="btn btn-primary" href = "/connect" method = "GET" role="button">Connexion</a>
    </div>
  </body>
  </html>
