<?php
class Appartenance{
    private int $leCreneau;
    private int $id;
    private int $numAttente;
    
    
    public function __construct(){}

    public function init($id,$leCreneau,$numAttente){
        if ($leCreneau > 0){
            $this->leCreneau = $leCreneau;
        }else{
            throw new Exception ("leCreneau invalide");
        }

        $this->id = $id;

        if ($numAttente >= 0){
            $this->numAttente = $numAttente;
        }else{
            throw new Exception ("lePrenom invalide");
        }

    }

    public function getId() : int{
        return $this->id;
    }
    public function getLeCreneau() : int{return $this->leCreneau;}
    public function getNumAttente() : int{return $this->numAttente;}
}


?>
