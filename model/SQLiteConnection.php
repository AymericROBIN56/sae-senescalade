<?php

class SQLiteConnection  {
    private static SQLiteConnection $con;
    private $connection;
        
    public function __construct(){
        try{
            $this->connection = new PDO('sqlite:sae.db');
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            echo($e->getMessage());
        }
    }

    public function getConnection() : PDO{
        return $this->connection;
    }



    public static function getInstance() : SQLiteConnection{
        if(!isset (self::$con)){
            self ::$con = new SQLiteConnection();
        }
        return self::$con;
    }

}

?>