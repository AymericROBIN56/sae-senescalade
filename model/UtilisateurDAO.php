<?php
class UtilisateurDAO{
    private static UtilisateurDAO $dao;


    private function __construct(){}

    public static function getInstance() : UtilisateurDAO{
        if(!isset (self::$dao)){
            self ::$dao = new UtilisateurDAO();
        }
        return self::$dao;
    }


    public final function findAll() : Array{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT * from Utilisateur ORDER BY 'nom','prenom'";
        $stmt = $connect->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_CLASS,'Utilisateur');

        return $result;
    }


    public final function find(string $mail) : Array{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT * from Utilisateur WHERE courriel = '$mail'";
        $stmt = $connect->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_CLASS,'Utilisateur');

        return $result;
    }


    public final function findByIdentite(string $mail,string $nom,string $prenom) : int{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT id from Utilisateur WHERE courriel = '$mail' AND nom = '$nom' AND prenom = '$prenom'";
        $stmt = $connect->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchColumn();

        return $result;
    }

    public final function findPren(string $nom,string $prenom) : int {
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT id from Utilisateur WHERE nom = '$nom' AND prenom = '$prenom'";
        $stmt = $connect->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchColumn();

        return $result;

    }

    public final function insert (Utilisateur $st) : void{
        if($st instanceof Utilisateur){ 
            $connect = SQLiteConnection::getInstance()->getConnection();
            $query = "INSERT INTO Utilisateur (
                courriel,action,nom,prenom,dateNaissance,sexe,nationalite,adresse,complAdr,codePostal,ville,pays,telephone,mobile,mdp,admin,personnePrevenirNom,personnePrevenirPrenom,personnePrevenirTel,personnePrevenirCourriel,
                numeroLicence,typeLicence,assurance,ski,slackline,trail,vtt,opAssurance,statut) VALUES (:mail,:act,:nom,:prenom,:dateNaissance,:sexe,:nationalite,:adresse,:complAdr,:codePostal,:ville,:pays,:tel,:mobile,:mdp,:ad,:prevNom,:prevPrenom,:prevTel,:prevMail,:numLicence,:typeLicence,:assurance,:ski,:slackline,:trail,:vtt,:op,:statut)";
            $stmt = $connect->prepare($query);

            $stmt->bindValue(':mail',$st->getMail(),PDO::PARAM_STR);
            $stmt->bindValue(':mdp',$st->getMdp (),PDO::PARAM_STR);
            $stmt->bindValue(':ad',$st->getAdmin (),PDO::PARAM_STR);
            $stmt->bindValue(':act',$st->getAction(),PDO::PARAM_STR);
            $stmt->bindValue(':nom',$st->getNom(),PDO::PARAM_STR);
            $stmt->bindValue(':prenom',$st->getPrenom(),PDO::PARAM_STR);
            $stmt->bindValue(':dateNaissance',$st->getDate(),PDO::PARAM_STR);
            $stmt->bindValue(':sexe',$st->getSexe(),PDO::PARAM_STR);
            $stmt->bindValue(':nationalite',$st->getNationalite(),PDO::PARAM_STR);
            $stmt->bindValue(':adresse',$st->getAdresse(),PDO::PARAM_STR);
            $stmt->bindValue(':complAdr',$st->getcomplAdr(),PDO::PARAM_STR);
            $stmt->bindValue(':codePostal',$st->getCodePostal(),PDO::PARAM_STR);
            $stmt->bindValue(':ville',$st->getVille(),PDO::PARAM_STR);
            $stmt->bindValue(':pays',$st->getPays(),PDO::PARAM_STR);
            $stmt->bindValue(':tel',$st->getTel(),PDO::PARAM_STR);
            $stmt->bindValue(':mobile',$st->getMobile(),PDO::PARAM_STR);
            $stmt->bindValue(':prevNom',$st->getPrevNom(),PDO::PARAM_STR);
            $stmt->bindValue(':prevPrenom',$st->getPrevPrenom(),PDO::PARAM_STR);
            $stmt->bindValue(':prevTel',$st->getPrevTel(),PDO::PARAM_STR);
            $stmt->bindValue(':prevMail',$st->getPrevMail(),PDO::PARAM_STR);
            $stmt->bindValue(':numLicence',$st->getNumeroLicence(),PDO::PARAM_STR);
            $stmt->bindValue(':typeLicence',$st->getTypeLicence(),PDO::PARAM_STR);
            $stmt->bindValue(':assurance',$st->getAssurance(),PDO::PARAM_STR);
            $stmt->bindValue(':ski',$st->getSki(),PDO::PARAM_STR);
            $stmt->bindValue(':slackline',$st->getSlackline(),PDO::PARAM_STR);
            $stmt->bindValue(':trail',$st->getTrail(),PDO::PARAM_STR);
            $stmt->bindValue(':vtt',$st->getVTT(),PDO::PARAM_STR);
            $stmt->bindValue(':op',$st->getOpAssurance(),PDO::PARAM_STR);
            $stmt->bindValue(':statut',$st->getStatut(),PDO::PARAM_STR);            
            $stmt->execute();
        }
    }

    public final function delete(int $id) : void{
            $connect = SQLiteConnection::getInstance()->getConnection();
            $query = "DELETE FROM Utilisateur WHERE id = $id";
            $stmt = $connect->prepare($query);

            $stmt->execute();

    }


    public final function update(Utilisateur $st): void {
        if ($st instanceof Utilisateur){
            $connect = SQLiteConnection::getInstance()->getConnection();
            $query = "UPDATE Utilisateur SET action = :act,dateNaissance = :dateNaissance,admin = :ad, sexe = :sexe, nationalite = :nationalite, adresse = :adresse, complAdr = :complAdr, codePostal = :codePostal, ville = :ville, pays = :pays, telephone = :tel, courriel = :mail, personnePrevenirNom = :prevNom, personnePrevenirPrenom = :prevPrenom, personnePrevenirTel = :prevTel, personnePrevenirCourriel = :prevMail, numeroLicence = :numLicence, typeLicence = :typeLicence, assurance = :assurance, ski = :ski, slackline = :slackline, trail = :trail, vtt = :vtt, opAssurance = :op WHERE prenom = :prenom  AND nom = :nom";
            $stmt = $connect->prepare($query);

            $stmt->bindValue(':mail',$st->getMail(),PDO::PARAM_STR);
            $stmt->bindValue(':mdp',$st->getMdp(),PDO::PARAM_STR);
            $stmt->bindValue(':ad',$st->getAdmin(),PDO::PARAM_STR);
            $stmt->bindValue(':act',$st->getAction(),PDO::PARAM_STR);
            $stmt->bindValue(':nom',$st->getNom(),PDO::PARAM_STR);
            $stmt->bindValue(':prenom',$st->getPrenom(),PDO::PARAM_STR);
            $stmt->bindValue(':dateNaissance',$st->getDate(),PDO::PARAM_STR);
            $stmt->bindValue(':sexe',$st->getSexe(),PDO::PARAM_STR);
            $stmt->bindValue(':nationalite',$st->getNationalite(),PDO::PARAM_STR);
            $stmt->bindValue(':adresse',$st->getAdresse(),PDO::PARAM_STR);
            $stmt->bindValue(':complAdr',$st->getcomplAdr(),PDO::PARAM_STR);
            $stmt->bindValue(':codePostal',$st->getCodePostal(),PDO::PARAM_STR);
            $stmt->bindValue(':ville',$st->getVille(),PDO::PARAM_STR);
            $stmt->bindValue(':pays',$st->getPays(),PDO::PARAM_STR);
            $stmt->bindValue(':tel',$st->getTel(),PDO::PARAM_STR);
            $stmt->bindValue(':mobile',$st->getMobile(),PDO::PARAM_STR);
            $stmt->bindValue(':prevNom',$st->getPrevNom(),PDO::PARAM_STR);
            $stmt->bindValue(':prevPrenom',$st->getPrevPrenom(),PDO::PARAM_STR);
            $stmt->bindValue(':prevTel',$st->getPrevTel(),PDO::PARAM_STR);
            $stmt->bindValue(':prevMail',$st->getPrevMail(),PDO::PARAM_STR);
            $stmt->bindValue(':numLicence',$st->getNumeroLicence(),PDO::PARAM_STR);
            $stmt->bindValue(':typeLicence',$st->getTypeLicence(),PDO::PARAM_STR);
            $stmt->bindValue(':assurance',$st->getAssurance(),PDO::PARAM_STR);
            $stmt->bindValue(':ski',$st->getSki(),PDO::PARAM_STR);
            $stmt->bindValue(':slackline',$st->getSlackline(),PDO::PARAM_STR);
            $stmt->bindValue(':trail',$st->getTrail(),PDO::PARAM_STR);
            $stmt->bindValue(':vtt',$st->getVTT(),PDO::PARAM_STR);
            $stmt->bindValue(':op',$st->getOpAssurance(),PDO::PARAM_STR);

            $stmt->execute();   
        }
    }

    public final function findAdmin() : Array{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT * from Utilisateur WHERE admin = 1";
        $stmt = $connect->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_CLASS,'utilisateur');

        return $result;
    }

    public final function findNoAdmin() : Array{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT * from Utilisateur WHERE admin = 0";
        $stmt = $connect->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_CLASS,'utilisateur');

        return $result;
    }

    public final function changeStatut(string $statut,int $id){
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "UPDATE Utilisateur SET statut = '$statut' WHERE id = $id";
        $stmt = $connect->prepare($query);
        $stmt->execute();
    }

}


?>