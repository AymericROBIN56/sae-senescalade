<?php
class Utilisateur{
    private int $id;
    private string $courriel;
    private string $mdp;
    private int $admin;
    private string $action;
    private string $nom;
    private string $prenom;
    private string $dateNaissance;
    private string $sexe;
    private string $nationalite;
    private string $adresse;
    private string $complAdr;
    private int $codePostal;
    private string $ville;
    private string $pays;
    private string $telephone;    
    private string $mobile;
    private string $personnePrevenirNom;
    private string $personnePrevenirPrenom;
    private string $personnePrevenirTel;
    private string $personnePrevenirCourriel;
    private int $numeroLicence;
    private string $typeLicence;
    private string $assurance;
    private string $ski;
    private string $slackline;
    private string $trail;
    private string $vtt;
    private string $opAssurance;
    private string $statut;

    
    
    public function __construct(){}

    public function init($id,$action,$nom,$prenom,$dateNaissance,$sexe,$nationalite,$adresse,$complAdr,$codePostal,$ville,$pays,$telephone,$mobile,$courriel,$mdp,$admin,$personnePrevenirNom,$personnePrevenirPrenom,$personnePrevenirTel,$personnePrevenirCourriel,$numeroLicence,$typeLicence,$assurance,$ski,$slackline,$trail,$vtt,$opAssurance,$statut){


        if ($action == 'Renouvellement' || $action == 'Inscription'){
            $this->action = $action;
        }else{
            throw new Exception ("action invalide");
        }


        if ($nom != null){
            $this->nom = $nom;
        }else{
            throw new Exception ("nom invalide");
        }

        if ($prenom != null){
            $this->prenom = $prenom;
        }else{
            throw new Exception ("prenom invalide");
        }

        if ($dateNaissance != null){
            $this->dateNaissance = $dateNaissance;
        }else{
            throw new Exception ("dateNaissance invalide");
        }

        if ($sexe == 'H' || $sexe == 'F'){
            $this->sexe = $sexe;
        }else{
            throw new Exception ("sexe invalide");
        }

        if ($nationalite != null){
            $this->nationalite = $nationalite;
        }else{
            throw new Exception ("nationalite invalide");
        }

        if ($adresse != null){
            $this->adresse = $adresse;
        }else{
            throw new Exception ("adresse invalide");
        }

        if ($codePostal >= 0){
            $this->codePostal = $codePostal;
        }else{
            throw new Exception ("codePostal invalide");
        }

        if ($ville != null){
            $this->ville = $ville;
        }else{
            throw new Exception ("ville invalide");
        }


        if ($pays != null){
            $this->pays = $pays;
         }else{
            throw new Exception ("pays invalide");
        }

        if ($courriel != null){
            $this->courriel = $courriel;
        }else{
            throw new Exception ("courriel invalide");
        }

        if ($mdp != null && strlen($mdp) >= 7){
            $this->mdp = $mdp;
        }else{
            throw new Exception ("mdp invalide");
        }

        if ($admin == 0 || $admin == 1){
            $this->admin = $admin;
        }else{
            throw new Exception ("admin invalide");
        }

        if ($action == 'Inscription' && $numeroLicence == null){
            $this->numeroLicence = 123456;
        }else if ($numeroLicence != null && strlen($numeroLicence) == 6){
            $this->numeroLicence = $numeroLicence;
        }else{
            throw new Exception ("numéro licence invalide");
        }

        if ($typeLicence == 'J' || $typeLicence == 'A' || $typeLicence == 'F'){
            $this->typeLicence = $typeLicence;
        }else{
            throw new Exception ("typeLicence invalide");
        }

        if ($ski == 'OUI' || $ski == 'NON'){
            $this->ski = $ski;
        }else{
            throw new Exception ("ski invalide");
        }

        if ($slackline == 'OUI' || $slackline == 'NON'){
            $this->slackline = $slackline;
        }else{
            throw new Exception ("slackline invalide");
        }

        if ($trail == 'OUI' || $trail == 'NON'){
            $this->trail = $trail;
        }else{
            throw new Exception ("trail invalide");
        }

        if ($vtt == 'OUI' || $vtt == 'NON'){
            $this->vtt = $vtt;
        }else{
            throw new Exception ("vtt invalide");
        }

        if ($opAssurance == 'IJ1' || $opAssurance == 'IJ2' || $opAssurance =='IJ3' || $opAssurance == 'NON'){
            $this->opAssurance = $opAssurance;
        }else{
            throw new Exception ("option assurance invalide");
        }

        if($assurance == 'RC' || $assurance == 'B' || $assurance == 'B+' || $assurance == 'B++'){
            $this->assurance = $assurance;
        }else{
            throw new Exception ("assurance invalide");
        }

        $this->id = $id;
        $this->statut = $statut;
        $this->complAdr = $complAdr;
        $this->telephone = $telephone;
        $this->mobile = $mobile;
        $this->personnePrevenirNom = $personnePrevenirNom;
        $this->personnePrevenirPrenom = $personnePrevenirPrenom;
        $this->personnePrevenirTel = $personnePrevenirTel;
        $this->personnePrevenirCourriel = $personnePrevenirCourriel;
    }


    public function getId() : int {return $this->id;}
    public function getAction() : string{return $this->action; }
    public function getNom() : string{return $this->nom; }
    public function getPrenom() : string{return $this->prenom; }
    public function getDate() : string{return $this->dateNaissance; }
    public function getSexe() : string{return $this->sexe; }
    public function getNationalite() : string{return $this->nationalite; }
    public function getAdresse() : string{return $this->adresse; }
    public function getComplAdr() : string{return $this->complAdr; }
    public function getCodePostal() : int{return $this->codePostal; }
    public function getVille() : string{return $this->ville; }
    public function getPays() : string{return $this->pays; }
    public function getTel() : string{return $this->telephone; }
    public function getMobile() : string{return $this->mobile; }
    public function getMail() : string{return $this->courriel; }
    public function getMdp(): string {return $this->mdp;}
    public function getAdmin(): int{return $this->admin;}
    public function getPrevNom() : string{return $this->personnePrevenirNom; }
    public function getPrevPrenom() : string{return $this->personnePrevenirPrenom; }
    public function getPrevTel() : string{return $this->personnePrevenirTel; }
    public function getPrevMail() : string{return $this->personnePrevenirCourriel; }
    public function getNumeroLicence() : int{return $this->numeroLicence; }
    public function getTypeLicence() : string{return $this->typeLicence; }
    public function getAssurance() : string{return $this->assurance; }
    public function getSki() : string{return $this->ski; }
    public function getSlackline() : string{return $this->slackline; }
    public function getTrail() : string{return $this->trail; }
    public function getVTT() : string{return $this->vtt; }
    public function getOpAssurance() : string{return $this->opAssurance; }
    public function getStatut() : string {return $this->statut;}

}


?>