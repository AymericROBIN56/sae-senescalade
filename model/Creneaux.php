<?php
class Creneaux{
    private int $numero;
    private string $jour;
    private string $horaireDeb;
    private string $horaireFin;
    private string $categorie;
    private string $division;
    private int $prix;
    private int $capacite;
    private string $descr;
    private int $fileAttente;
    
    
    public function __construct(){}

    public function init($num,$jour,$horaireDeb,$horaireFin,$division,$categorie,$prix,$descr,$capacite,$fileAttente){
        
        if ($num >= 0){
            $this->numero = $num;
        }else{
            throw new Exception ("numero invalide");
        }



        if($jour == 'Lundi' || $jour == 'Mardi' || $jour == 'Mercredi' || $jour == 'Jeudi' || $jour == 'Vendredi' || $jour == 'Samedi' || $jour == 'Dimanche'){
            $this->jour = $jour;
        }else{
            throw new Exception ("jour invalide");
        }



        if ( $horaireDeb != null){
            $this->horaireDeb = $horaireDeb;
        }else{
            throw new Exception ("horaire de début invalide");
        }


        if ( $horaireFin != null){
            $this->horaireFin = $horaireFin;
        }else{
            throw new Exception ("horaire de fin invalide");
        }


        if ($division == 'U8' || $division == 'U10' || $division == 'U12' || $division == 'U14' || $division == 'U16' || $division == 'U18' || $division == 'U20' || $division == '+20'){
            $this->division = $division;
        }else{
            throw new Exception ("division invalide");
        }
        
        
        if ($capacite >= 0){
            $this->capacite = $capacite;
        }else{
            throw new Exception ("capacite invalide");
        }
        
        
        if ($fileAttente >= 0){
            $this->fileAttente = $fileAttente;
        }else{
            throw new Exception ("file attente invalide");
        }

        if($categorie != null){
            $this->categorie = $categorie;
        }else{
            throw new Exception("Catégorie invalide");
        }

        if($prix > 0) {
            $this->prix = $prix;
        }else{
            throw new Exception("Prix invalide");
        }
        $this->descr = $descr;
    }



    public function getNumero() : int{
        return $this->numero;
    }


    public function getJour(): string{
        return $this->jour;
    }


    public function getHoraireDeb() : string {
        return $this->horaireDeb;
    }

    public function getHoraireFin() : string {
        return $this->horaireFin;
    }

    public function getCat() : string{return $this->categorie;}
    public function getDiv() : string{return $this->division;}
    public function getCapacite() : int{return $this->capacite;}
    public function getPrix() : int{return $this->prix;}
    public function getDescr() : string{return $this->descr;}
    public function getFileAttente() : int{return $this->fileAttente;}
}


?>
