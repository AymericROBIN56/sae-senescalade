<?php
class CreneauxDAO{
    private static CreneauxDAO $dao;


    private function __construct(){}

    public static function getInstance() : CreneauxDAO{
        if(!isset (self::$dao)){
            self ::$dao = new CreneauxDAO();
        }
        return self::$dao;
    }


    public final function findAll() : Array{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT * from Creneaux ORDER BY 'numero'";
        $stmt = $connect->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_CLASS,'creneaux');

        return $result;
    }


    public final function find(int $numero) : Array{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT * from Creneaux WHERE numero = '$numero'";
        $stmt = $connect->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_CLASS,'creneaux');
        return $result;
    }

    public final function findByDiv(string $div) : Array{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT * from Creneaux WHERE division = '$div'";
        $stmt = $connect->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_CLASS,'creneaux');
        return $result;
    }
    

    public final function capacite(int $numero) : int {
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT capacite FROM Creneaux WHERE numero = '$numero'";
        $stmt = $connect->query($query);
        $result = $stmt->fetchColumn();
        return $result;
    }


    public final function attente(int $numero) : int {
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT fileAttente FROM Creneaux WHERE numero = $numero";
        $stmt = $connect->query($query);
        $result = $stmt->fetchColumn();
        return $result;
    }

    public final function insert (Creneaux $st) : void{
        if($st instanceof Creneaux){
            $connect = SQLiteConnection::getInstance()->getConnection();
            $query = "INSERT INTO Creneaux(jour,horaireDeb,horaireFin,division,categorie,capacite,description,prix,fileAttente) VALUES (:jour,:horaireDeb,:horaireFin,:div,:cat,:capacite,:descr,:prix,:file)";
            $stmt = $connect->prepare($query);

            $stmt->bindValue(':jour',$st->getJour(),PDO::PARAM_STR);
            $stmt->bindValue(':horaireDeb',$st->getHoraireDeb(),PDO::PARAM_STR);
            $stmt->bindValue(':horaireFin',$st->getHoraireFin(),PDO::PARAM_STR);
            $stmt->bindValue(':div',$st->getDiv(),PDO::PARAM_STR);
            $stmt->bindValue(':cat',$st->getCat(),PDO::PARAM_STR);
            $stmt->bindValue(':prix',$st->getPrix(),PDO::PARAM_STR);
            $stmt->bindValue(':descr',$st->getDescr(),PDO::PARAM_STR);
            $stmt->bindValue(':capacite',$st->getCapacite(),PDO::PARAM_STR);
            $stmt->bindValue(':file',$st->getFileAttente(),PDO::PARAM_STR);

            
            $stmt->execute();
        }
    }

    public final function delete(int $numero) : void{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "DELETE FROM Creneaux WHERE numero = $numero";
        $stmt = $connect->prepare($query);

        $stmt->execute();

    }


    public final function update(Creneaux $st): void {
        if ($st instanceof Creneaux){
            $connect = SQLiteConnection::getInstance()->getConnection();
            $query = "UPDATE Creneaux SET jour = :jour, horaires = :horaires, division = :div, nbPersonne = :pers WHERE numero = :num ";
            $stmt = $connect->prepare($query);

            $stmt->bindValue(':num',$st->getNumero(),PDO::PARAM_STR);
            $stmt->bindValue(':jour',$st->getJour(),PDO::PARAM_STR);
            $stmt->bindValue(':horaires',$st->getHoraires(),PDO::PARAM_STR);
            $stmt->bindValue(':horaires',$st->getDiv(),PDO::PARAM_STR);
            $stmt->bindValue(':pers',$st->getNbPersonne(),PDO::PARAM_STR);

            $stmt->execute();   
        }
    }
}


?>
