<?php
class AppartenanceDAO{
    private static AppartenanceDAO $dao;


    private function __construct(){}

    public static function getInstance() : AppartenanceDAO{
        if(!isset (self::$dao)){
            self ::$dao = new AppartenanceDAO();
        }
        return self::$dao;
    }


    public final function findAll() : Array{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT * from Appartenance ORDER BY leCreneau";
        $stmt = $connect->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_CLASS,'appartenance');

        return $result;
    }


    public final function find(int $idUser,int $leCreneau) : Array{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT * from Appartenance WHERE userId = '$id' AND leNom = '$leNom'";
        $stmt = $connect->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_CLASS,'appartenance');

        return $result;
    }

    public final function getAttente(int $leCreneau,int $id) : int{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT numeroAttente FROM Appartenance WHERE userId = '$id' AND leCreneau = '$leCreneau'";
        $stmt = $connect->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchColumn();

        return $result;
    }


    public final function getCreneauxUser(int $id) : Array{
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "SELECT leCreneau from Appartenance WHERE userId = '$id'";
        $stmt = $connect->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_CLASS,'appartenance');

        return $result;
    }
    public final function insert (Appartenance $st) : void{
        if($st instanceof Appartenance){
            $connect = SQLiteConnection::getInstance()->getConnection();
            $query = "INSERT INTO Appartenance VALUES (:id,:creneau,:attente)";
            $stmt = $connect->prepare($query);

            $stmt->bindValue(':id',$st->getId(),PDO::PARAM_STR);
            $stmt->bindValue(':creneau',$st->getLeCreneau(),PDO::PARAM_STR);
            $stmt->bindValue(':attente',$st->getNumAttente(),PDO::PARAM_STR);

            
            $stmt->execute();
        }
    }

    public final function delete(int $id,int $numero) : void{
        
        $connect = SQLiteConnection::getInstance()->getConnection();
        $query = "DELETE FROM Appartenance WHERE userId = '$id' AND leCreneau = $numero";
        $stmt = $connect->prepare($query);
        $stmt->execute();

    }

}


?>
