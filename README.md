Senescalade - Application Web pour les Cours d'Escalade
Description
Senescalade est une application web développée en PHP suivant le modèle MVC (Modèle-Vue-Contrôleur). Cette application vise à faciliter la gestion et la réservation de cours d'escalade. Que vous soyez débutant ou expérimenté, Senescalade offre une plateforme conviviale pour explorer, réserver et suivre vos cours d'escalade.

Fonctionnalités
Gestion des Utilisateurs: Les utilisateurs peuvent créer un compte, se connecter et mettre à jour leurs informations personnelles.

Cours d'Escalade: Consultez la liste complète des cours d'escalade disponibles ainsi que les horaires.

Réservations: Réservez facilement des cours d'escalade qui correspondent à vos disponibilités.

Profil Utilisateur: Chaque utilisateur dispose d'un profil où il peut consulter son historique de cours et gérer ses informations.

Administration: Les administrateurs peuvent gérer les cours, les utilisateurs et visualiser des rapports sur l'activité de l'application.

Prérequis
Serveur web (Apache, Nginx, etc.)
PHP version 7.0 ou supérieure
Base de données SQLite3
Installation
Clonez ce dépôt dans le répertoire de votre serveur web.
Utilisé cette commande : git clone https://github.com/votre-utilisateur/senescalade.git

Lancez l'application dans votre navigateur en accédant à l'URL correspondante.

Structure du Projet
controllers/: Contient les contrôleurs de l'application qui permettent la navigation entre les pages.

model/: Contient les codes permettant de manipuler la base de données.

static/: Fichiers publics accessibles via le navigateur (styles, scripts, images, etc.).

views/: Fichiers permettant de visualiser les pages.

sae: Base de données.

Script bdd: Script de création de la base de données.

index: Fichier contenant les chemin entre les controllers et les vues.

Contributeurs : 
ROBIN Aymeric
MONBOUSSIN Dewi
SECRETIN Nathan

