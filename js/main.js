const App={
    data() {
        return {
            showCours : false,
            showMain : true,
            Adultes : true,
            Jeunes : false,
            Compet : false,
            Baby : false,
        }
    },
    methods: {

    cours(){
        this.showCours = true,
        this.showMain = false
    },
    toAdultes(){
        this.Adultes = true,
        this.Jeunes = false,
        this.Compet = false,
        this.Baby = false
    },
    toJeunes(){
        this.Adultes = false,
        this.Jeunes = true,
        this.Compet = false,
        this.Baby = false
    },
    toCompet(){
        this.Adultes = false,
        this.Jeunes = false,
        this.Compet = true,
        this.Baby = false
    },
    toBaby(){
        this.Adultes = false,
        this.Jeunes = false,
        this.Compet = false,
        this.Baby = true
    }
        
    },

}



Vue.createApp(App).mount('#app')