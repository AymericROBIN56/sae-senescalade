<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
define ("__ROOT__",__DIR__);

// Configuration
require_once (__ROOT__.'/config.php');

// ApplicationController
require_once (CONTROLLERS_DIR.'/ApplicationController.php');



// Add routes here
ApplicationController::getInstance()->addRoute('connect', CONTROLLERS_DIR.'/connect.php');
ApplicationController::getInstance()->addRoute('user_add', CONTROLLERS_DIR.'/UserAddController.php');
ApplicationController::getInstance()->addRoute('connect', CONTROLLERS_DIR.'/ConnectUserController.php');
ApplicationController::getInstance()->addRoute('creneau', CONTROLLERS_DIR.'/CreneauController.php');
ApplicationController::getInstance()->addRoute('creneau_add', CONTROLLERS_DIR.'/AjoutCreneau.php');
ApplicationController::getInstance()->addRoute('update', CONTROLLERS_DIR.'/UpdateUser.php');
ApplicationController::getInstance()->addRoute('accueil', CONTROLLERS_DIR.'/AccueilController.php');
ApplicationController::getInstance()->addRoute('manage_admin', CONTROLLERS_DIR.'/ManageAdmin.php');
ApplicationController::getInstance()->addRoute('add_admin', CONTROLLERS_DIR.'/AddAdministrateur.php');
ApplicationController::getInstance()->addRoute('del_admin', CONTROLLERS_DIR.'/DeleteAdmin.php');
ApplicationController::getInstance()->addRoute('del_user', CONTROLLERS_DIR.'/DeleteUser.php');
ApplicationController::getInstance()->addRoute('manage_user', CONTROLLERS_DIR.'/ManageUser.php');
ApplicationController::getInstance()->addRoute('contact', CONTROLLERS_DIR.'/ContactController.php');
ApplicationController::getInstance()->addRoute('choix', CONTROLLERS_DIR.'/ChoixCreneauxController.php');
ApplicationController::getInstance()->addRoute('manage_creneau', CONTROLLERS_DIR.'/ManageCreneaux.php');
ApplicationController::getInstance()->addRoute('del_creneau', CONTROLLERS_DIR.'/DeleteCreneaux.php');
ApplicationController::getInstance()->addRoute('confirm_desinscr',CONTROLLERS_DIR .'/ConfirmDesinscr.php');
ApplicationController::getInstance()->addRoute('disconnect',CONTROLLERS_DIR .'/DisconnectController.php');
ApplicationController::getInstance()->addRoute('mail_entry',CONTROLLERS_DIR .'/MailEntryController.php');
ApplicationController::getInstance()->addRoute('family',CONTROLLERS_DIR .'/FamilyController.php');
ApplicationController::getInstance()->addRoute('update_cren',CONTROLLERS_DIR .'/UpdateCren.php');     


// Process the request
ApplicationController::getInstance()->process();

?>
